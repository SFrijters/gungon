# GunGon notes

## Geometry: intersections and collisions

* Bullet: DLine
* Corpse: DCircle
* Enemy: NGon, fold over DLine (convex)
* Player: NGon, fold over DLine (convex)
* Terrain: DLines, fold over DLine (not necessarily convex)

| Things    |  Bullet   |  Corpse   |   Enemy   |  Player   |  Terrain  |
| --------- |:---------:|:---------:|:---------:|:---------:|:---------:|
| Bullet    |  ignore   |  ignore   | intersect | intersect | intersect |
| Corpse    |  ignore   |  ignore   | intersect | intersect | intersect |
| Enemy     | intersect | intersect | collision | collision | collision |
| Player    | intersect | intersect | collision | collision | collision |
| Terrain   | intersect | intersect | collision | collision | collision |
