{
  description = "GunGon: Guns on 'Gons";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixgl = {
      url = "github:guibou/nixGL";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs =
    {
      nixpkgs,
      flake-utils,
      nixgl,
      ...
    }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (
      system:
      let
        # Version of GunGon
        version = "0.1.0";

        pkgs = import nixpkgs {
          inherit system;
          config = {
            # Use allowUnfree to allow unfree fonts
            allowUnfree = true;
          };
          overlays = [
            # Custom SDL2 with fewer features to significantly reduce closure size
            (final: prev: {
              SDL2 = prev.SDL2.override {
                alsaSupport = false;
                waylandSupport = false;
                drmSupport = false;
                dbusSupport = false;
                udevSupport = false;
                libdecorSupport = false;
                pipewireSupport = false;
                pulseaudioSupport = false;
                withStatic = false;
              };
            })
            nixgl.overlay
          ];
        };

        inherit (pkgs) lib;

        # Wrapper for OpenGL + nix
        # Using pkgs.nixgl.auto.nixGLDefault is impure
        nixGLexe = "nixGLIntel";
        nixGL = pkgs.nixgl.${nixGLexe};

        # Select GHC compiler version
        ghcVersion = "ghc983";

        # Haskell package set with chosen ghc version
        haskellPackages = pkgs.haskell.packages.${ghcVersion}.override {
          # (Temporary) fixes for 9.8
          overrides =
            let
              inherit (pkgs.haskell.lib.compose)
                dontCheck
                doJailbreak
                appendPatch
                overrideSrc
                addExtraLibraries
                addBuildDepends
                markUnbroken
                overrideCabal
                ;
            in
            # https://github.com/haskell-game/sdl2/issues/307
            self: super: { sdl2 = doJailbreak super.sdl2; };
        };

        # Derivation for the GunGon package itself
        # The `gungon.nix` file is generated manually from `gungon.cabal` using `cabal2nix . > gungon.nix`
        gungon = pkgs.haskell.lib.overrideCabal (haskellPackages.callPackage ./gungon.nix { }) (drv: {
          prePatch = ''
            substituteInPlace src/GunGon/Render/Font.hs \
              --replace "res/fonts/rubik/Rubik-Regular.ttf" "${pkgs.rubik}/share/fonts/truetype/Rubik-Regular.ttf" \
              --replace "res/fonts/dejavu/DejaVuSans.ttf" "${pkgs.dejavu_fonts}/share/fonts/truetype/DejaVuSans.ttf"

            # Replace version with nix version
            {
              echo "{-# LANGUAGE OverloadedStrings #-}"
              echo "module GunGon.Config.Version (version) where"
              echo "import Data.Text (Text)"
              echo "version :: Text"
              echo "version = \"${version}\""
            } > src/GunGon/Config/Version.hs
          '';

          src = lib.fileset.toSource {
            root = ./.;
            fileset = lib.fileset.unions [
              ./cabal.project
              ./gameConfig.json
              ./gungon.cabal
              ./LICENSE
              ./Setup.hs
              ./app
              ./benchmark
              ./res
              ./src
              ./test
            ];
          };

          # Add fonts
          libraryHaskellDepends = drv.libraryHaskellDepends ++ [
            pkgs.dejavu_fonts
            pkgs.rubik
          ];

          postInstall = ''
            mkdir -p $out/share/gungon
            cp gameConfig.json $out/share/gungon
          '';
        });

        gungon-static-only = pkgs.haskell.lib.justStaticExecutables gungon;

        # Yeah, this is nasty, tests should pass, but since the update to GHC 9.6.3 and beyond the test suite is flaky
        gungon-no-tests = pkgs.haskell.lib.compose.dontCheck gungon;
        gungon-static-only-no-tests = pkgs.haskell.lib.compose.dontCheck gungon-static-only;

        # Wrapper that calls the GunGon executable through nixGL and points to the installed default config
        gungon-nixGL-and-config-wrapper = pkgs.writeShellScriptBin "gungon-nixGL-and-config-wrapper" ''
          exec ${nixGL}/bin/${nixGLexe} ${pkgs.lib.getExe gungon-static-only-no-tests} --config-path "${gungon-static-only-no-tests}/share/gungon/gameConfig.json" "$@"
        '';

        # GunGon with tests and benchmarks - for development
        gungon-dev = pkgs.haskell.lib.overrideCabal gungon (
          drv:
          drv
          // {
            doCheck = true;
            doBenchmark = true;
          }
        );

        # Development shell for GunGon
        # Can run `cabal build` inside this shell
        gungon-devshell = haskellPackages.shellFor {
          packages = _: [ gungon-dev ];

          nativeBuildInputs = [
            haskellPackages.cabal-install
            haskellPackages.cabal2nix
            nixGL
          ];

          withHoogle = true;

          shellHook = ''
            name=gungon-dev-env
            mkdir -p res/fonts
            rm -f res/fonts/dejavu
            ln -s ${pkgs.dejavu_fonts}/share/fonts/truetype res/fonts/dejavu
            rm -f res/fonts/rubik
            ln -s ${pkgs.rubik}/share/fonts/truetype res/fonts/rubik

            alias gungon='${nixGL}/bin/${nixGLexe} cabal run gungon-exe --'
          '';
        };

        # Docker image containing only GunGon
        gungon-container = pkgs.dockerTools.buildLayeredImage {
          name = "gungon";
          tag = "nix";
          # created = "now";
          contents = [ gungon-nixGL-and-config-wrapper ];
          config.Cmd = [
            "${gungon-nixGL-and-config-wrapper}/bin/gungon-nixGL-and-config-wrapper"
            "--check-config"
          ];
        };

        # (Interactive) docker image for development and CI
        gungon-dev-container-args =
          let
            gungon-dev-env = haskellPackages.ghcWithPackages (
              _: gungon-dev.buildInputs ++ gungon-dev.propagatedBuildInputs
            );
            imageInteractivePkgs = with pkgs; [
              bashInteractive
              coreutils-full
              git
              gnugrep
              cabal-install
              cabal2nix
            ];
          in
          {
            name = "gungon-dev";
            tag = "nix";
            # created = "now";
            contents = [
              gungon-dev-env
              imageInteractivePkgs
            ];

            extraCommands = ''
              mkdir -p etc/nix
              echo "experimental-features = nix-command flakes" > etc/nix/nix.conf
              mkdir -p tmp
              mkdir -p root/.cabal && touch root/.cabal/config
              mkdir -p share/fonts/truetype/dejavu
              ln -s ${pkgs.dejavu_fonts}/share/fonts/truetype share/fonts/dejavu
              mkdir -p share/fonts/truetype/rubik
              ln -s ${pkgs.rubik}/share/fonts/truetype share/fonts/rubik
            '';
          };

        gungon-dev-container = pkgs.dockerTools.buildLayeredImage gungon-dev-container-args;
        gungon-dev-container-stream = pkgs.dockerTools.streamLayeredImage gungon-dev-container-args;

        # Development shell for interacting with containers
        # Based on https://gist.github.com/adisbladis/187204cb772800489ee3dac4acdd9947
        gungon-podman-devshell =
          let
            # Provides a script that copies/creates files that are required for rootless podman
            podmanSetupScript =
              let
                registriesConf = pkgs.writeText "registries.conf" ''
                  [registries.search]
                  registries = ['docker.io']
                  [registries.block]
                  registries = []
                '';
                storageConf = pkgs.writeText "storage.conf" ''
                  [storage]
                  driver = "overlay"
                  # rootless_storage_path="$XDG_DATA_HOME/containers/storage"
                '';
              in
              pkgs.writeShellScript "podman-setup" ''
                # Dont overwrite customised configuration
                if ! test -f ~/.config/containers/policy.json; then
                  echo "Installing missing ~/.config/containers/policy.json"
                  install -Dm644 ${pkgs.skopeo.src}/default-policy.json ~/.config/containers/policy.json
                fi
                if ! test -f ~/.config/containers/registries.conf; then
                  echo "Installing missing ~/.config/containers/registries.conf"
                  install -Dm644 ${registriesConf} ~/.config/containers/registries.conf
                fi
                if ! test -f ~/.config/containers/storage.conf; then
                  echo "Installing missing ~/.config/containers/storage.conf"
                  install -Dm644 ${storageConf} ~/.config/containers/storage.conf
                fi
              '';

          in
          pkgs.mkShell {
            name = "gungon-podman";

            packages = with pkgs; [
              podman # Manage pods, containers and images
              runc # Container runtime
              conmon # Container runtime monitor
              skopeo # Interact with container registry
              slirp4netns # User-mode networking for unprivileged namespaces
              fuse-overlayfs # CoW for images, much faster than default vfs
            ];

            shellHook = ''
              # Install configuration required for rootless podman
              ${podmanSetupScript}

              if ! grep -q "^''${USER}:" /etc/subuid; then
                echo "No subuid range defined for user, consider running 'sudo usermod --add-subuids 10000-75535 ''${USER}' to allow rootless podman to work"
              fi

              # Interactions with GitLab container registry (for CI)
              export CI_REGISTRY=registry.gitlab.com
              if test -f ./gitlab-container-registry-credentials.sh; then
                echo "Loading GitLab registry credentials, logging in"
                source ./gitlab-container-registry-credentials.sh
                podman login -u "''${CI_REGISTRY_USER}" -p "''${CI_REGISTRY_PASSWORD}" "''${CI_REGISTRY}"
              else
                echo "GitLab registry credential file 'gitlab-container-registry-credentials.sh' does not exist"
                echo "Consider creating a file containing"
                echo "export CI_REGISTRY_USER=..."
                echo "export CI_REGISTRY_PASSWORD=..."
              fi

              # Interactions with GitLab
              alias gungon-podman-gitlab-login='podman login -u "''${CI_REGISTRY_USER}" -p "''${CI_REGISTRY_PASSWORD}" "''${CI_REGISTRY}"'
              alias gungon-podman-gitlab-update='./update-gitlab-image.sh'

              # Reset local podman completely (dangerous!)
              alias gungon-podman-system-reset='podman system reset'

              # List all containers with 'gungon' in the name
              alias gungon-podman-list-containers='podman ps --all --storage | grep "gungon\|^CONTAINER ID"'
              # Remove all containers with 'gungon' in the name
              alias gungon-podman-remove-containers='podman ps --all --storage | tail -n +2 | grep gungon | awk "{print \$1}" | xargs podman rm'
              # List all images with 'gungon' in the name
              alias gungon-podman-list-images='podman image ls | grep "gungon\|^REPOSITORY"'
              # Remove all images with 'gungon' in the name
              alias gungon-podman-remove-images='podman image ls | tail -n +2 | grep gungon | awk "{print \$3}" | xargs podman image rm -f'

              # Build the dev container
              alias gungon-podman-build-dev-container='nix build .#gungon-dev-container -o result-gungon-dev-container'
              # Build the container
              alias gungon-podman-build-container='nix build .#gungon-container -o result-gungon-container'

              # Load the generated dev container tarball
              alias gungon-podman-load-dev-container='podman load -i ./result-gungon-dev-container'
              # Load the generated container tarball
              alias gungon-podman-load-container='podman load -i ./result-gungon-container'

              # Enter the dev container available on GitLab
              alias gungon-podman-enter-dev-remote='podman run -it -v $(pwd):/mnt "''${CI_REGISTRY}/$(echo "''${CI_REGISTRY_USER}" | tr '[:upper:]' '[:lower:]')/gungon:nix sh'
              # Enter the dev container available on localhost
              alias gungon-podman-enter-dev-local='podman run -it -v $(pwd):/mnt localhost/gungon-dev:nix sh'
              # Enter the container available on localhost
              alias gungon-podman-enter-local='podman run -it -v $(pwd):/mnt localhost/gungon:nix'

            '';
          };
      in
      rec {
        apps.gungon = {
          type = "app";
          program = "${pkgs.lib.getExe gungon-nixGL-and-config-wrapper}";
        };

        apps.default = apps.gungon;

        packages = {
          inherit
            gungon
            gungon-dev
            gungon-no-tests
            gungon-static-only
            gungon-static-only-no-tests
            gungon-nixGL-and-config-wrapper
            gungon-container
            gungon-dev-container
            gungon-dev-container-stream
            ;
          inherit (haskellPackages) ghc;
        };

        packages.default = packages.gungon-static-only;

        devShells = {
          gungon = gungon-devshell;
          gungon-podman = gungon-podman-devshell;
        };

        devShells.default = devShells.gungon;

        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
