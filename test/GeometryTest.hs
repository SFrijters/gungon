{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE TypeSynonymInstances #-}

module GeometryTest
  ( prop_intersectInsideTriangle
  , unit_intersectInsideTriangle
  , prop_intersectInsideQuadrangle
  , prop_intersectInsideSweep
  , prop_intersectInsideCircle
  ) where

import           GunGon.Geometry.Convex
import           GunGon.Types

import           Test.Tasty.HUnit
import           Test.Tasty.QuickCheck

{-# ANN module "HLint: ignore Use camelCase" #-}

instance Arbitrary a => Arbitrary (V2 a) where
  arbitrary = V2 <$> arbitrary <*> arbitrary

instance Arbitrary a => Arbitrary (Point V2 a) where
  arbitrary = P <$> arbitrary

prop_intersectInsideTriangle :: DLine -> DTriangle -> Bool
prop_intersectInsideTriangle l@(p1, p2) t =
  validIntersection (intersectSegTriangle l t) p1Inside p2Inside
  where
    p1Inside = pointInsideTriangle p1 t
    p2Inside = pointInsideTriangle p2 t

unit_intersectInsideTriangle :: IO ()
unit_intersectInsideTriangle = do
  check t (P $ V2 (-1.0) 0.5 ) (P $ V2   1.0  0.5 ) False False
  check t (P $ V2 (-1.0) 0.5 ) (P $ V2   0.0  0.5 ) False True
  check t (P $ V2 (-1.0) 0.5 ) (P $ V2 (-0.5) 0.5 ) False False
  check t (P $ V2   0.0  0.25) (P $ V2   0.5  0.25) True  True
  where
    check t' p1 p2 i1 i2 = do
      let l = (p1, p2)
      pointInsideTriangle p1 t' @?= i1
      pointInsideTriangle p2 t' @?= i2
      prop_intersectInsideTriangle l t' @?= True
    t = (P $ V2 (-0.5) 0, P $ V2 1 0, P $ V2 0 1)

prop_intersectInsideQuadrangle :: DLine -> DQuadrangle -> Bool
prop_intersectInsideQuadrangle l@(p1, p2) q =
  not (isConvex $ quadrangleSegs q) || -- only valid for convex quadrangles
  validIntersection (intersectSegQuadrangle l q) p1Inside p2Inside
  where
    p1Inside = pointInsideQuadrangle p1 q
    p2Inside = pointInsideQuadrangle p2 q

prop_intersectInsideSweep :: DLine -> DLine -> DVec -> Bool
prop_intersectInsideSweep l@(p1, p2) sl sv =
  validIntersection (intersectSegSweep l sl sv) p1Inside p2Inside
  where
    p1Inside = pointInsideSweep p1 sl sv
    p2Inside = pointInsideSweep p2 sl sv

prop_intersectInsideCircle :: DLine -> DCircle -> Bool
prop_intersectInsideCircle l@(p1, p2) c@(_, cr) =
  cr < 0 || -- only valid for circles with positive radius
  validIntersection (intersectSegCircle l c) p1Inside p2Inside
  where
    p1Inside = pointInsideCircle p1 c
    p2Inside = pointInsideCircle p2 c

validIntersection :: IntersectSegConvex -> Bool -> Bool -> Bool
validIntersection SCNoIntersect    i1 i2 = not $ xor i1 i2
validIntersection SCOneIntersect{} i1 i2 = xor i1 i2
validIntersection SCTwoIntersect{} i1 i2 = not i1 && not i2

xor :: Bool -> Bool -> Bool
xor x y = (x && not y) || (not x && y)
