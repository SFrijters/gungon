{
  mkDerivation,
  aeson,
  base,
  bytestring,
  containers,
  criterion,
  file-embed,
  formatting,
  gitrev,
  lens,
  lib,
  linear,
  monad-logger,
  mtl,
  optparse-applicative,
  random,
  sdl2,
  sdl2-ttf,
  tasty,
  tasty-discover,
  tasty-hunit,
  tasty-quickcheck,
  text,
  time,
  Unique,
}:
mkDerivation {
  pname = "gungon";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson
    base
    bytestring
    containers
    file-embed
    formatting
    gitrev
    lens
    linear
    monad-logger
    mtl
    optparse-applicative
    random
    sdl2
    sdl2-ttf
    text
    time
    Unique
  ];
  executableHaskellDepends = [ base ];
  testHaskellDepends = [
    base
    tasty
    tasty-discover
    tasty-hunit
    tasty-quickcheck
  ];
  testToolDepends = [ tasty-discover ];
  benchmarkHaskellDepends = [
    base
    criterion
  ];
  homepage = "https://gitlab.com/SFrijters/gungon";
  license = lib.licenses.bsd3;
  mainProgram = "gungon-exe";
}
