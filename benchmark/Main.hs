--{-# LANGUAGE DeriveTraversable          #-}
--{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Main where

import           Criterion.Main

-- From SDL
newtype Point f a = P (f a)
  --  deriving ( Eq, Ord, Show, Read, Monad, Functor, Applicative, Foldable
  --           , Traversable, Fractional, Num)
-- From Linear
data V2 a = V2 !a !a
  -- deriving (Eq, Ord, Show, Read)

type DPoint      = Point V2 Double
type DLine       = (DPoint, DPoint)

data IntersectSegSeg = SSNoIntersect | SSOneIntersect DPoint -- deriving Show

intersectSegSeg :: DLine -> DLine -> IntersectSegSeg
intersectSegSeg (P (V2 p1x p1y), P (V2 p2x p2y)) (P (V2 q1x q1y), P (V2 q2x q2y)) =
  if s >= 0 && s <= 1 && t >= 0 && t <= 1 && (abs det > 1e-20) then
    SSOneIntersect intersection
  else
    SSNoIntersect
  where
    s1x = p2x - p1x
    s1y = p2y - p1y
    s2x = q2x - q1x
    s2y = q2y - q1y
    det = -s2x * s1y + s1x * s2y
    s = (-s1y * (p1x - q1x) + s1x * (p1y - q1y)) / det
    t = ( s2x * (p1y - q1y) - s2y * (p1x - q1x)) / det
    intersection = P $ V2 (p1x + (t * s1x)) (p1y + (t * s1y))

main :: IO ()
main = do
  let p1 = P $ V2 (-1) 0
      p2 = P $ V2 1 0
      p3 = P $ V2 (-1) 1
      p4 = P $ V2 1 (-1)
      p5 = P $ V2 1 1
      l1 = (p1, p2) -- horizontal
      l2 = (p2, p4) -- diagonal
      l3 = (p3, p5) -- horizontal
  defaultMain [ bgroup "intersectSegSeg" [
                  bench "one" $ whnf (intersectSegSeg l1) l2
                  ,bench "no"  $ whnf (intersectSegSeg l1) l3]]
