# GunGon

![CI status](https://gitlab.com/SFrijters/gungon/badges/master/pipeline.svg)

!["Gameplay" screenshot](screenshot.png)

If you have Nix with flakes installed, just `nix run` (from a local clone) or `nix run gitlab:SFrijters/gungon` (from anywhere); otherwise, see below.

## Gameplay

Gameplay is pretty much non-existent as of this moment: the enemies don't even move or shoot back and there is no scoring system. But you can blow them up!

### Controls

- Accelerate / decelerate / turn with the arrow keys. The blue line inside your 'Gon points towards its front.
- Shoot with the LMB or spacebar.
- You will shoot from the segment of your 'Gon that is facing your mouse cursor.
- Press 'f' to toggle fullscreen mode.
- Press escape to pause / unpause.
- Press 'q' to quit.

## Building and running

The Nix way: install [Nix](https://nixos.org/nix/download.html) with [flake support](https://github.com/mschwaig/howto-install-nix-with-flake-support).

Using only cabal: untested, but should work (?).

### Building

```console
$ nix build
```

or (inside the `nix develop` shell):

```console
$ cabal build
```

The Nix way uses `gungon.nix` to determine dependencies. We can update the `gungon.nix` file from `gungon.cabal` using (inside the `nix develop` shell):

```console
$ cabal2nix . > gungon.nix
```

Note: We could use `haskellPackages.cabal2nix` inside the flake, but I want to avoid [IFD](https://nixos.wiki/wiki/Import_From_Derivation) for now.
Maybe we will have [cabal2nix without IFD](https://github.com/cdepillabout/cabal2nixWithoutIFD) in the future.

### Running

```console
$ nix run
```

or (inside the `nix develop` shell):

```console
$ cabal run gungon-exe
```

Running `gungon-exe` in a Nix shell may result in

```
gungon-exe: SDLCallFailed {sdlExceptionCaller = "SDL.Video.createRenderer", sdlFunction = "SDL_CreateRenderer", sdlExceptionError = "Couldn't find matching render driver"}
```

To avoid this, use the `gungon` alias instead (available in the `nix develop` shell).

Use the `-d` flag to show extra debug output.

### Testing

Inside the `nix develop` shell:

```console
$ cabal test
```

## CI / Docker (podman)

CI configuration is set up in [.gitlab-ci.yml](.gitlab-ci.yml).
It defines two CI jobs: one running `cabal` inside a container, and one running `nix build` on a Nix-capable machine.

To modify / build / interact with containers an alternative development shell is provided:

```console
$ nix develop .#gungon-podman
```

Various useful command aliases are provided, prefixed with `gungon-podman-`.
