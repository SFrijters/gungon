{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TemplateHaskell        #-}

module GunGon.Config.GameConfig where

import           Control.Lens         (makeFields)
import           Data.ByteString.Lazy (readFile)
import           Data.Map             (Map)
import           GHC.Generics
import           Prelude              hiding (readFile)

import           GunGon.Data.JSON
import           GunGon.Entity.Types

data GameConfig = GameConfig
  { _gameConfigPlayerTypes    :: Map String PlayerConfig
  , _gameConfigEnemyTypes     :: Map String EnemyConfig
  , _gameConfigModuleTypes    :: Map String ModuleTypeConfig
  , _gameConfigCoreTypes      :: Map String NGonCoreConfig
  , _gameConfigCorpseConfig   :: CorpseConfig
  , _gameConfigTerrainConfig  :: [TerrainConfig]
  , _gameConfigInitialPlayer  :: (String, (Double, Double))
  , _gameConfigInitialEnemies :: [(String, (Double, Double))]
  , _gameConfigSpawnChance    :: Double
  , _gameConfigRandomSeed     :: Int
  } deriving (Generic, Show)
makeFields ''GameConfig

instance ToJSON GameConfig where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_gameConfig" }

instance FromJSON GameConfig where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_gameConfig" }

readGameConfig :: FilePath -> IO (Either String GameConfig)
readGameConfig configPath = eitherDecode <$> readFile configPath
