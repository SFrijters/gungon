{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TemplateHaskell        #-}

{-# LANGUAGE ApplicativeDo          #-}
{-# LANGUAGE RecordWildCards        #-}

module GunGon.Config.OptParse where

import           Control.Lens         (makeFields)
import           Control.Monad.Logger (LogLevel (..))
import           Options.Applicative

data Options = Options
  { _optionsDoShowVersion :: Bool
  , _optionsDoCheckConfig :: Bool
  , _optionsLogLevel      :: LogLevel
  , _optionsDoDebug       :: Bool
  , _optionsConfigPath    :: String
  } deriving Show
makeFields ''Options

mkParserOptions :: Parser Options
mkParserOptions = do
  _optionsLogLevel <-
    option auto $
    short 'v' <>
    long "log-level" <>
    help "Log level to use" <>
    showDefault <>
    value LevelInfo
  _optionsDoShowVersion <-
    switch $
    long "version" <>
    help "Show version number and exit"
  _optionsDoCheckConfig <-
    switch $
    long "check-config" <>
    help "Try to parse the config file and exit"
  _optionsDoDebug <-
    switch $
    short 'd' <>
    long "debug" <>
    help "Show debug information"
  _optionsConfigPath <-
    strOption $
    short 'c' <>
    long "config-path" <>
    help "Which JSON config file to use" <>
    showDefault <>
    value "gameConfig.json"
  pure Options {..}

getCmdOptions :: IO Options
getCmdOptions = execParser opts
  where
    opts = info (mkParserOptions <**> helper) ( fullDesc
     <> progDesc "GunGon - A Game of Guns and Polygons"
     <> header "gungon - a game of guns and polygons" )
