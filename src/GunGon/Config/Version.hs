{-# LANGUAGE TemplateHaskell #-}

module GunGon.Config.Version (version) where

import           Data.Text          (Text, pack)
import           Development.GitRev (gitDescribe)

version :: Text
version = pack $(gitDescribe)
