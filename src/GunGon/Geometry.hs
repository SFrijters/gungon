module GunGon.Geometry where

import           Linear       (angle, norm, perp, signorm, (*^))

import           GunGon.Types

unit :: DVec
unit = V2 1 0

rotUnit :: Double -> DVec
rotUnit theta = rotate theta unit

rotate :: Double -> DVec -> DVec
rotate theta (V2 x y) = V2 (x * cos theta - y * sin theta) (x * sin theta + y * cos theta)

segTranslate :: DVec -> DLine -> DLine
segTranslate v (p1, p2) = (p1 + P v, p2 + P v)

segTranslatePerp :: Double -> DLine -> DLine
segTranslatePerp shift seg = segTranslate vec seg
  where vec = shift *^ segPerp seg

segVec :: DLine -> DVec
segVec (P v1, P v2) = v2 - v1

-- segUnit :: DLine -> DVec
-- segUnit = signorm . segVec

segAngle :: DLine -> Double
segAngle (P v1, P v2) = atan2 dy dx
  where V2 dx dy = v1 - v2

segLen :: DLine -> Double
segLen (P v1, P v2) = norm (v1-v2)

segMid :: DLine -> DPoint
segMid (P (V2 x1 y1), P (V2 x2 y2)) = P ( V2 ((x1+x2) / 2) ((y1+y2) / 2))

segPerp :: DLine -> DVec
segPerp (P v1, P v2) = signorm $ perp $ v1 - v2

segExtend :: Double -> Double -> DLine -> DLine
segExtend d1 d2 l@(p1, p2) = (p1 - P v1, p2 + P v2)
  where
    v1 = d1 *^ (signorm . segVec $ l)
    v2 = d2 *^ (signorm . segVec $ l)

segRotate :: Double -> DLine -> DLine
segRotate theta l = segRotateAround theta (segMid l) l

segRotateAround :: Double -> DPoint -> DLine -> DLine
segRotateAround theta (P (V2 xp yp)) (P (V2 x1 y1), P (V2 x2 y2)) =
  (P $ V2 (x1'+xp) (y1'+yp), P $ V2 (x2'+xp) (y2'+yp))
  where
    V2 x1' y1' = rotate theta $ V2 (x1-xp) (y1-yp)
    V2 x2' y2' = rotate theta $ V2 (x2-xp) (y2-yp)

polygonPoints :: Int -> DPoint -> Double -> Double -> [DPoint]
polygonPoints n pos size rot = map (shift . P . scale . rotate rot ) points
  where
    shift     = (pos +)
    scale     = (size *^)
    points    = map angle $ take n $ enumFromThen 0.0 anglestep
    anglestep = 2.0*pi / fromIntegral n

polygonLines :: Int -> DPoint -> Double -> Double -> [DLine]
polygonLines n pos size rot = closedSegs $ polygonPoints n pos size rot

closedSegs :: [DPoint] -> [DLine]
closedSegs []  = []
closedSegs [_] = []
closedSegs ps@(_:pt) = zip ps (pt ++ ps)

openSegs :: [DPoint] -> [DLine]
openSegs []  = []
openSegs [_] = []
openSegs ps@(_:pt)  = zip ps pt

noline :: DLine
noline = (P $ V2 0 0, P $ V2 0 0)
