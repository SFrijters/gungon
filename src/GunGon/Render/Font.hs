{-# LANGUAGE TemplateHaskell #-}

module GunGon.Render.Font ( createFontMap
                          , lookupFont
                          , Font
                          , PointSize
                          , FontMap
                          , FontKey
                          , FontFace(..)) where

import           Control.Monad.IO.Class (MonadIO (..))
import           Data.ByteString        (ByteString)
import           Data.FileEmbed         (embedOneFileOf)
import qualified Data.Map.Strict        as Map
import           SDL.Font               (Font, PointSize, decode)

type FontKey = (FontFace, PointSize)
type FontMap = Map.Map FontKey Font

data FontFace =
    RubikRegular
  | DejaVuSans
  deriving (Show, Ord, Eq, Enum, Bounded)

createFontMap :: MonadIO m => m (Map.Map FontKey Font)
createFontMap = do
  let fontFaces  = [ minBound :: FontFace .. ]
      pointSizes = [ 6..24 ]
      keys = [ (fontFace, pointSize) | fontFace <- fontFaces, pointSize <- pointSizes ]
  values <- mapM decodeFont keys
  return $ Map.fromList (zip keys values)

lookupFont :: FontKey -> FontMap -> Font
lookupFont fk fm = Map.findWithDefault defaultFont fk fm
  where defaultFont = fm Map.! (DejaVuSans, 16)

decodeFont :: MonadIO m => FontKey -> m Font
decodeFont (ff, ps) = decode (embeddedFont ff) ps

embeddedFont :: FontFace -> ByteString
embeddedFont RubikRegular = $(embedOneFileOf [
                            "res/fonts/rubik/Rubik-Regular.ttf"
                            ])
embeddedFont DejaVuSans   = $(embedOneFileOf [
                            "res/fonts/dejavu/DejaVuSans.ttf"
                            ])
