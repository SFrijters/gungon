module GunGon.Render.Color
  ( module GunGon.Render.Color
  , module SDL.Raw.Types ) where

import           SDL.Raw.Types       (Color (..))

import           GunGon.Entity.Types

withAlpha :: Word8 -> Color -> Color
withAlpha a (Color r b g _) = Color r b g a

fromNGonColor :: NGonColor -> Color
fromNGonColor White   = white
fromNGonColor Red     = red
fromNGonColor Magenta = magenta

powerColor :: Double -> Color
powerColor p
  | p > 256.0 = magenta
  | p > 224.0 = blue
  | p > 192.0 = cyan
  | p > 160.0 = green
  | p > 128.0 = yellow
  | p >  64.0 = red
  | otherwise = darkgray

moduleColor :: ModuleType -> Color
moduleColor (Armor'  _) = yellow
moduleColor (Shield' _) = blue
moduleColor (Gun'    _) = red
moduleColor  Collector' = green
moduleColor  NoModule'  = darkgray

white    :: Color
white     = Color 255 255 255 255
red      :: Color
red       = Color 255 0   0   255
green    :: Color
green     = Color 0   255 0   255
blue     :: Color
blue      = Color 0   0   255 255
yellow   :: Color
yellow    = Color 255 255 0   255
magenta  :: Color
magenta   = Color 255 0   255 255
cyan     :: Color
cyan      = Color 0   255 255 255
black    :: Color
black     = Color 0   0   0   255
darkgray :: Color
darkgray  = Color 128 128 128 255
trans    :: Color
trans     = Color 0   0   0   0
