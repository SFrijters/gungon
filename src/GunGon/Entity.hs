module GunGon.Entity where

import           Control.Lens           ((%~), (&), (.~), (^.))
import           Data.List              (zipWith4)
import           Linear                 ((*^))

import           GunGon.Entity.NGon
import           GunGon.Entity.Types
import           GunGon.Geometry
import           GunGon.Geometry.Convex

class Entity a where
  integrate :: TimeStep -> a -> a
  isExpired :: a -> Bool

instance Entity Bullet where
  integrate dt b =
    b & lineOld .~ b^.line
      & line    %~ segExtend (b^.dSizeLeft * dt) (b^.dSizeRight * dt) . segTranslate (b^.speed * dt *^ b^.dir)
      & time    %~ (+) dt
  isExpired b = (b^.time) > (b^.lifeTime)

instance Entity Corpse where
  integrate dt cor = cor & segments .~ segments'
                         & time %~ (+) dt
    where segments' = map updateSeg zipped
          zipped = zip3 (cor^.segments) (cor^.segVelocities) (cor^.segRotations)
          updateSeg ((p1, p2), v, r) = segRotate (dt*r) (p1 + dt *^ P v, p2 + dt *^ P v)
  isExpired corpse = corpse^.time > corpse^.coreLifeTime && corpse^.time > corpse^.segLifeTime

instance Entity NGon where
  integrate dt ng =
    ng & pos          .~ pos'
       & energy       %~ (\en -> min (ng^.core.maxEnergy) (en + dt * ng^.core.dEnergy))
       & moduleSides  %~ fmap (updateSideCooldowns dt) . zipWith4 updateSideSegments segments' segmentsOld' moduleSegments'
    where pos' = ng^.pos + dt *^ P (ng^.vel)
         -- TODO: change this call to something using fmap
          segments'       = polygonLines (length $ ng^.moduleSides) pos' (ng^.size) (ng^.rot)
          segmentsOld'    = fmap (^.segment) (ng^.moduleSides)
          moduleSegments' = innerSegments (0.1*ng^.size) segments'
  isExpired ng = ng^.isDying

instance Entity Enemy where
  integrate dt e = e & nGon %~ integrate dt
  isExpired e = e^.nGon.isDying

instance Entity Player where
  integrate dt p = p & nGon %~ integrate dt
  isExpired p = p^.nGon.isDying
