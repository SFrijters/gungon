{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TemplateHaskell            #-}

module GunGon.Render where

import qualified SDL
import qualified SDL.Font

import           Control.Lens           (makeFields, (&), (.~), (^.))
import           Control.Monad          (unless, when)
import           Control.Monad.IO.Class (MonadIO (..))
import           Control.Monad.Reader   (MonadReader, ReaderT, ask, asks,
                                         runReaderT)
import           Data.Text              (Text, null, pack, unpack)
import           Formatting             (fixed, sformat, stext, (%))
import           Linear                 (V4 (..), dot, signorm, (*^), (^+^))
import           Prelude                hiding (null)

import qualified GunGon.Config.Version
import           GunGon.Entity.Bullet
import           GunGon.Entity.NGon
import           GunGon.Entity.Types
import           GunGon.Geometry
import           GunGon.Render.Color
import           GunGon.Render.Font
import           GunGon.State.Types

data GRenderer = GRenderer
  { _gRendererRenderer      :: SDL.Renderer
  , _gRendererWindow        :: SDL.Window
  , _gRendererFontMap       :: FontMap
  , _gRendererDoRenderDebug :: Bool
  , _gRendererCameraOffset  :: DVec
  , _gRendererWindowSize    :: DVec
  }
makeFields ''GRenderer

class (MonadReader GRenderer m, MonadIO m) => Renderer m

initialize :: MonadIO m => Bool -> m GRenderer
initialize doRenderDebug' = do
  SDL.initialize [SDL.InitVideo]
  SDL.Font.initialize

  window' <- SDL.createWindow "GunGon" SDL.defaultWindow
  renderer' <- SDL.createRenderer window' (-1) SDL.defaultRenderer
  SDL.rendererDrawBlendMode renderer' SDL.$= SDL.BlendAlphaBlend
  fontMap' <- createFontMap
  windowSize' <- fmap dVec $ SDL.get $ SDL.windowSize window'

  return GRenderer
    { _gRendererRenderer      = renderer'
    , _gRendererWindow        = window'
    , _gRendererFontMap       = fontMap'
    , _gRendererDoRenderDebug = doRenderDebug'
    , _gRendererCameraOffset  = V2 0 0
    , _gRendererWindowSize    = windowSize'
    }

destroy :: MonadIO m => GRenderer -> m ()
destroy gr = do
  SDL.destroyRenderer $ gr^.renderer
  SDL.destroyWindow   $ gr^.window
  SDL.Font.quit
  SDL.quit

clearRenderer :: Renderer m => m ()
clearRenderer = setRendererColor (Color 0 0 0 255) >> (SDL.clear =<< asks _gRendererRenderer)

presentRenderer :: Renderer m => m ()
presentRenderer = SDL.present =<< asks _gRendererRenderer

setRendererColor :: Renderer m => Color -> m ()
setRendererColor (Color r g b a) = do
  gr <- ask
  SDL.rendererDrawColor (gr^.renderer) SDL.$= V4 r g b a

toggleWindowMode :: Renderer m => m ()
toggleWindowMode = do
  window' <- asks _gRendererWindow
  curMode <- SDL.windowMode <$> SDL.getWindowConfig window'
  SDL.setWindowMode window' (toggle curMode)
  where toggle SDL.Windowed = SDL.FullscreenDesktop
        toggle _            = SDL.Windowed

setCameraOffset :: MonadIO m => SceneState -> GRenderer -> m GRenderer
setCameraOffset sceneState_ gRenderer_ = do
    wsize <- SDL.get $ SDL.windowSize (gRenderer_^.window)
    let (P playerPosition) = sceneState_^.gameState.player.nGon.pos
        screenCenter = dVec $ (`div` 2) <$> wsize
        cameraOffset' = screenCenter - playerPosition
        windowSize' = dVec wsize
    return $ gRenderer_ & cameraOffset .~ cameraOffset'
                        & windowSize   .~ windowSize'

getSceneSize :: MonadIO m => GRenderer -> m DVec
getSceneSize gRenderer_ = fmap dVec (SDL.get $ SDL.windowSize (gRenderer_^.window))

class Renderable a where
  render :: Renderer m => a -> m ()

instance Renderable Bullet where
  render b = do
    cameraOffset' <- asks (^.cameraOffset)
    renderLine c (segTranslate cameraOffset' $ b^.line)
    where
      c = powerColor $ 2.0 * powerDensity b

instance Renderable Enemy where
  render en = do
    cameraOffset' <- asks (^.cameraOffset)
    renderCircle magenta (en^.nGon.pos + P cameraOffset') (en^.nGon.size * en^.nGon.core.relSize)
    mapM_ (renderLine (fromNGonColor $ en^.nGon.color) . segTranslate cameraOffset' . (^.segment)) (en^.nGon.moduleSides)
    mapM_ (uncurry renderLine . (\side -> (moduleColor $ side^.moduleType, segTranslate cameraOffset' $ side^.moduleSegment))) $ en^.nGon.moduleSides
    doRenderDebug' <- asks (^.doRenderDebug)
    when doRenderDebug' $ let fontSize = max 6 (min (round $ 0.1 * en^.nGon.size) 24 ) in
      renderText Center (DejaVuSans, fontSize) white (iPoint $ en^.nGon.pos + P cameraOffset') (formatDouble $ en^.nGon.energy)

instance Renderable Corpse where
  render corpse = do
    cameraOffset' <- asks (^.cameraOffset)
    renderCircle green' (corpse^.corePos + P cameraOffset') (corpse^.coreSize)
    mapM_ (renderLine white' . segTranslate cameraOffset') (corpse^.segments)
    where white' = withAlpha (round $ 255 * (corpse^.segLifeTime  - corpse^.time) / corpse^.segLifeTime) white
          green' = withAlpha (round $ 255 * (corpse^.coreLifeTime - corpse^.time) / corpse^.coreLifeTime) green

instance Renderable Player where
  render p = do
    cameraOffset' <- asks (^.cameraOffset)
    renderCircle green (p^.nGon.pos + P cameraOffset') (p^.nGon.size * p^.nGon.core.relSize)
    mapM_ (uncurry renderLine . (\side -> (c $ side^.isActive, segTranslate cameraOffset' $ side^.segment))) $ p^.nGon.moduleSides
    mapM_ (uncurry renderLine . (\side -> (moduleColor $ side^.moduleType, segTranslate cameraOffset' $ side^.moduleSegment))) $ p^.nGon.moduleSides
    renderLine blue $ segTranslate cameraOffset' (p^.nGon.pos, getFront $ p^.nGon)
      where c a = if a then green else white

instance Renderable Terrain where
  render terrain_ = do
    cameraOffset' <- asks (^.cameraOffset)
    mapM_ (renderLineDashed [(white, 5.0), (trans, 5.0)]) $ segTranslate cameraOffset' . (^.segment) <$> terrain_^.moduleSides
    mapM_ (uncurry renderLine . (\side -> (moduleColor $ side^.moduleType, segTranslate cameraOffset' $ side^.moduleSegment))) $ terrain_^.moduleSides

renderPolygon :: Renderer m => [Color] -> [DPoint] -> m ()
renderPolygon _ [] = return ()
renderPolygon _ (_:[]) = return ()
renderPolygon c points@(_:pointst) =
  mapM_ renderLine' (zip3 c points (pointst ++ points))
  where renderLine' (c', p1, p2) = renderLine c' (p1, p2)

-- TODO: Change this to a better algorithm, so that we can have arcs too
renderCircle :: Renderer m => Color -> DPoint -> Double -> m ()
renderCircle c p r = renderPolygon (repeat c) $ polygonPoints 20 p r 0

-- renderArc :: GRenderer -> DPoint -> Double -> (Double, Double) -> IO ()
-- renderArc gr p r (theta1, theta2) = undefined

renderLine :: Renderer m => Color -> DLine -> m ()
renderLine c (p1, p2) = do
  gr <- ask
  when (inWindow (gr^.windowSize) p1 || inWindow (gr^.windowSize) p2) $
    setRendererColor c >> SDL.drawLine (gr^.renderer) (iPoint p1) (iPoint p2)

inWindow :: DVec -> DPoint -> Bool
inWindow (V2 wx wy) (P (V2 x y)) = x >= 0 && y >= 0 && x <= wx && y <= wy

renderLineDashed :: Renderer m => [(Color, Double)] -> DLine -> m ()
renderLineDashed pattern_ segment_@(p1, p2) = mapM_ (uncurry renderLine) lines'
  where
    direction = signorm $ segVec segment_
    points = takeWhile (\p -> dot (p - p1) (p - p2) <= 0) $ scanl (\p l -> p ^+^ (P $ l *^ direction)) p1 (snd <$> cycle pattern_) ++ [p2]
    lines' = zip (fst <$> cycle pattern_) (openSegs points)

data TextAnchor = TopLeft | TopRight | BottomLeft | BottomRight | Center

renderDefaultText :: Renderer m => IPoint -> Text -> m ()
renderDefaultText = renderText Center (DejaVuSans, 16) white

renderText :: Renderer m => TextAnchor -> FontKey -> Color -> IPoint -> Text -> m ()
renderText a fk c p t = unless (null t) $ do
  gr <- ask
  let font = lookupFont fk (gr^.fontMap)
      fc = fontColor c
  textSurface <- SDL.Font.solid font fc t
  textTexture <- SDL.createTextureFromSurface (gr^.renderer) textSurface
  SDL.freeSurface textSurface
  (width, height) <- SDL.Font.size font t
  let s = toEnum <$> V2 width height
  let rect = SDL.Rectangle (topLeftPos p s a) s
  SDL.copy (gr^.renderer) textTexture Nothing (Just rect)
  SDL.destroyTexture textTexture
  where topLeftPos p'           _        TopLeft     = p'
        topLeftPos (P (V2 x y)) (V2 w _) TopRight    = P $ V2 (x - w)  y
        topLeftPos (P (V2 x y)) (V2 _ h) BottomLeft  = P $ V2  x      (y - h)
        topLeftPos (P (V2 x y)) (V2 w h) BottomRight = P $ V2 (x - w) (y - h)
        topLeftPos (P (V2 x y)) (V2 w h) Center      = P $ V2 (x - (w `div` 2)) (y - (h `div` 2))
        fontColor (Color r g b alpha) = V4 r g b alpha

renderCursor :: Renderer m => DPoint -> m ()
renderCursor (P (V2 x y)) = renderPolygon cs points
  where
    points = fmap P points'
    points' = [ V2 (x + curSize) (y + curSize)
              , V2 (x - curSize) (y + curSize)
              , V2 (x - curSize) (y - curSize)
              , V2 (x + curSize) (y - curSize) ]
    cs = repeat cyan
    curSize = 20

newtype SceneRenderer m a = SceneRenderer (ReaderT GRenderer m a)
  deriving (Functor, Applicative, Monad, MonadReader GRenderer, MonadIO)

instance MonadIO m => Renderer (SceneRenderer m)

runRenderer :: GRenderer -> SceneRenderer m a -> m a
runRenderer gr (SceneRenderer m) = runReaderT m gr

renderScene :: Renderer m => SceneState -> Double -> m ()
renderScene ss fps = do
  when (ss^.doToggleWM) toggleWindowMode
  clearRenderer
  renderFPS fps
  case ss^.scene of
    SceneGame -> do
      renderGameState $ ss^.gameState
      renderDebug $ ss^.gameState
      renderCursor $ ss^.mousePos
    SceneGameOver -> do
      renderGameState $ ss^.gameState
      renderVersion
      renderGameOver
    SceneGameStart -> do
      renderVersion
      renderGameStart
    ScenePause -> do
      renderGameState $ ss^.gameState
      renderVersion
      renderPause
    SceneQuit -> do
      renderQuit
      renderVersion
    SceneTitle -> do
      renderTitle
      renderVersion
    _ -> return ()
  presentRenderer

getTextSize :: Renderer m => FontKey -> Text -> m IVec
getTextSize fk t = do
  gr <- ask
  (width, height) <- SDL.Font.size (lookupFont fk (gr^.fontMap)) t
  return $ toEnum <$> V2 width height

renderGameState :: Renderer m => GameState -> m ()
renderGameState gs = do
  mapM_ render $ gs^.bullets
  mapM_ render $ gs^.corpses
  mapM_ render $ gs^.enemies
  mapM_ render $ gs^.terrain
  unless (gs^.playerIsDead) $ render (gs^.player)

renderVersion :: Renderer m => m ()
renderVersion = do
  gr <- ask
  when (gr^.doRenderDebug) $ do
    wsize <- SDL.get $ SDL.windowSize (gr^.window)
    renderText BottomRight (DejaVuSans, 16) white (P wsize) GunGon.Config.Version.version

renderFPS :: Renderer m => Double -> m ()
renderFPS = renderText TopLeft (DejaVuSans, 16) white (P (V2 0 0)) . debugText
  where debugText = sformat("fps = " % fixed 2)

renderDebug :: Renderer m => GameState -> m ()
renderDebug gameState' = do
  gr <- ask
  when (gr^.doRenderDebug) $ renderText TopLeft (DejaVuSans, 16) white (P (V2 0 20)) (debugText gameState')
    where debugText gs' = sformat("p = " % stext % ", v = " % stext % ", rot = " % fixed 2 % ", e = " % fixed 2 % ", dead = " % stext)
            (formatPoint $ gs'^.player.nGon.pos)
                          (formatPoint $ P $ gs'^.player.nGon.vel)
                          (gs'^.player.nGon.rot)
                          (gs'^.player.nGon.energy)
                          (pack $ show $ gs'^.player.nGon.isDying)

renderTwoLines :: Renderer m => Text -> Text -> m ()
renderTwoLines t1 t2 = do
  gr <- ask
  wsize <- SDL.get $ SDL.windowSize (gr^.window)
  let centrePos = P $ fmap (`div` 2) wsize
  renderText Center (DejaVuSans, 64) white centrePos t1
  (V2 _ dy) <- getTextSize (DejaVuSans, 32) t2
  let subPos = P $ fmap (`div` 2) wsize ^+^ V2 0 dy
  renderText Center (DejaVuSans, 32) red subPos t2

renderGameOver :: Renderer m => m ()
renderGameOver = renderTwoLines "Game Over" "Press space to restart"

renderGameStart :: Renderer m => m ()
renderGameStart = renderTwoLines "Loading..." ""

renderPause :: Renderer m => m ()
renderPause = renderTwoLines "Paused" "Press escape to continue"

renderQuit :: Renderer m => m ()
renderQuit = renderTwoLines "Exiting" "Thanks for playing"

renderTitle :: Renderer m => m ()
renderTitle = renderTwoLines "GunGon" "Press space to start"

formatPoint :: DPoint -> Text
formatPoint (P (V2 x y)) = sformat ("(" % fixed 2 % "," % fixed 2 % ")") x y

formatDouble :: Double -> Text
formatDouble = sformat (fixed 2)

printVersion :: IO ()
printVersion = putStrLn $ unpack GunGon.Config.Version.version
