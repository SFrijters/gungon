{-# LANGUAGE OverloadedStrings #-}

module GunGon.Data.JSON
  ( module GunGon.Data.JSON
  , module Data.Aeson )
where

import           Data.Aeson (FromJSON (..), ToJSON (..), constructorTagModifier,
                             defaultOptions, eitherDecode, fieldLabelModifier,
                             genericParseJSON, genericToJSON)
import           Data.Char  (toLower)
import           Data.Text  (pack, replace, unpack)

configFieldLabelModifier :: String -> String -> String
configFieldLabelModifier prefix = firstToLower . drop (length prefix)
  where firstToLower []     = []
        firstToLower (x:xs) = toLower x : xs

configConstructorTagModifier :: String -> String
configConstructorTagModifier = firstToLower . unpack . replace "Config'" "" . pack
  where firstToLower []     = []
        firstToLower (x:xs) = toLower x : xs
