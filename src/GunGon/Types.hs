module GunGon.Types ( IPoint, DPoint, IVec, DVec, ILine, DLine, DTriangle, DQuadrangle, DCircle
                    , MousePos, TimeStep
                    , Point(..), V2(..)
                    , CInt, CDouble, Word8
                    , dPoint, iPoint, dVec, iVec, cDouble
                    , pairsOf) where

import           Foreign.C.Types (CDouble, CInt)
import           GHC.Word        (Word8)
import           Linear          (V2 (..))
import           SDL             (Point (..))

type IPoint      = Point V2 CInt
type DPoint      = Point V2 Double
type IVec        = V2 CInt
type DVec        = V2 Double
type ILine       = (IPoint, IPoint)
type DLine       = (DPoint, DPoint)
type DTriangle   = (DPoint, DPoint, DPoint)
type DQuadrangle = (DPoint, DPoint, DPoint, DPoint)
type DCircle     = (DPoint, Double)

type MousePos = DPoint

type TimeStep = Double

dVec :: IVec -> DVec
dVec = fmap fromIntegral

iVec :: DVec -> IVec
iVec = fmap round

dPoint :: IPoint -> DPoint
dPoint = fmap fromIntegral

iPoint :: DPoint -> IPoint
iPoint = fmap round

cDouble :: Double -> CDouble
cDouble = realToFrac

pairsOf :: [a] -> [(a,a)]
pairsOf (x1:x2:xs) = (x1,x2):pairsOf xs
pairsOf _          = []
