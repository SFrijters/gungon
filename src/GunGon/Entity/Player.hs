module GunGon.Entity.Player where

import           Control.Lens             ((%~), (&), (.~), (^.))
import           Data.Map                 (lookup)
import           Linear                   ((*^))
import           Prelude                  hiding (lookup)

import           GunGon.Config.GameConfig
import           GunGon.Entity.Bullet
import           GunGon.Entity.NGon
import           GunGon.Entity.Types
import           GunGon.Geometry
import           GunGon.Geometry.Convex

spawnPlayer :: GameConfig -> String -> DPoint -> Player
spawnPlayer gameConfig_ playerKey_ pos_ = getPlayer gameConfig_ playerKey_ & nGon . pos .~ pos_

getPlayer :: GameConfig -> String -> Player
getPlayer gameConfig playerKey = maybe mkDummyPlayer (mkPlayer gameConfig) $ lookup playerKey (gameConfig^.playerTypes)

mkPlayer :: GameConfig -> PlayerConfig -> Player
mkPlayer gameConfig_ playerConfig_ = mkDummyPlayer & nGon .~ mkNGon gameConfig_ (playerConfig_^.nGon)

mkDummyPlayer :: Player
mkDummyPlayer = Player { _playerNGon = mkDummyNGon }

spawnInitialPlayer :: GameConfig -> Player
spawnInitialPlayer gameConfig_ = spawn $ gameConfig_^.initialPlayer
  where spawn (playerKey, (x, y)) = spawnPlayer gameConfig_ playerKey (P $ V2 x y)

accelerate :: Double -> Player -> Player
accelerate dt p = p & nGon.vel %~ (\v -> v + p^.nGon.core.dSpeed*dt *^ rotUnit (p^.nGon.rot))

rotate :: Double -> Player -> Player
rotate dt p = p & nGon.rot %~ (\r -> r + dt* (p^.nGon.core.dRot))

updatePlayerIsDying :: Player -> Player
updatePlayerIsDying p = p & nGon . isDying .~ (p^.nGon.energy < 0 || p^.nGon.isDying)

updatePlayerActiveSides :: DPoint -> NGon -> NGon
updatePlayerActiveSides mousePosRel_ nGon_ = nGon_ & moduleSides %~ fmap (\side -> side & isActive .~ playerSideIsActive (nGon_^.pos) mousePosRel_ side)

playerSideIsActive :: DPoint -> DPoint -> ModuleSide -> Bool
playerSideIsActive np mpRel side = isIntersectSegSeg (np, mpRel) (side^.segment)

hitBulletsPlayer :: ([Bullet], Player) -> ([Bullet], Player)
hitBulletsPlayer (bs, p) = (bs', p')
  where
    ng = p^.nGon
    (bs', ng') = hitBulletsNGon (bs, ng)
    p' = p & nGon .~ ng'
