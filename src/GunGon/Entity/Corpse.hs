module GunGon.Entity.Corpse where

import           Control.Lens             ((&), (.~), (^.))
import           Linear                   ((*^))
import           System.Random            (StdGen, randomRs)

import           GunGon.Config.GameConfig
import           GunGon.Entity.NGon
import           GunGon.Entity.Types

spawnCorpse :: GameConfig -> StdGen -> NGon -> Corpse
spawnCorpse gameConfig stdGen nGon_ =
  corpse' & corePos       .~ nGon_^.pos
          & coreSize      .~ nGon_^.size * nGon_^.core.relSize
          & coreEnergy    .~ gameConfig^.corpseConfig.energyFraction * nGon_^.core.maxEnergy
          & segments      .~ getSegments nGon_
          & segVelocities .~ randomVelocities'
          & segRotations  .~ randomRotations'
  where corpse' = mkCorpse gameConfig (gameConfig^.corpseConfig)
        n = length $ nGon_^.moduleSides -- TODO: use fmap instead
        (rrots', rvels') = splitAt n $ randomRs (-1, 1) stdGen
        randomRotations'  = fmap (0.25*pi*) rrots'                             -- TODO: Rotation speed parameter
        randomVelocities' = (\(l1, l2) -> 50.0 *^ V2 l1 l2) <$> pairsOf rvels' -- TOOD: Translation speed parameter

mkCorpse :: GameConfig -> CorpseConfig -> Corpse
mkCorpse _ corpseConfig_ =
  mkDummyCorpse & coreSize     .~ corpseConfig_^.coreSize
                & coreLifeTime .~ corpseConfig_^.coreLifeTime
                & segLifeTime  .~ corpseConfig_^.segLifeTime

mkDummyCorpse :: Corpse
mkDummyCorpse = Corpse
  { _corpseCorePos       = P $ V2 0 0
  , _corpseCoreEnergy    = 0.0
  , _corpseCoreLifeTime  = 0.0
  , _corpseCoreSize      = 0.0
  , _corpseSegments      = []
  , _corpseSegVelocities = []
  , _corpseSegRotations  = []
  , _corpseTime          = 0.0
  , _corpseSegLifeTime   = 0.0
  }

updateDead :: GameConfig -> StdGen -> [NGon] -> [Corpse] -> [Corpse]
updateDead gameConfig stdGen nGons corpses = fmap (spawnCorpse gameConfig stdGen) nGons <> corpses

removeDeadCorpses :: [Corpse] -> [Corpse]
removeDeadCorpses = filter (\corpse -> corpse^.time < corpse^.coreLifeTime && corpse^.time < corpse^.segLifeTime)

removeCorpseDeadSegments :: Corpse -> Corpse
removeCorpseDeadSegments corpse =
  if
    corpse^.time > corpse^.segLifeTime
  then
    corpse & segments .~ []
  else
    corpse
