{-# LANGUAGE FlexibleContexts #-}

module GunGon.Entity.NGon where

import           Control.Lens             ((%~), (&), (.~), (^.), (^?))
import           Control.Lens.Extras      (is)
import           Data.List                ()
import           Data.Map                 (lookup)
import           Prelude                  hiding (lookup)

import           GunGon.Config.GameConfig
import           GunGon.Entity.ModuleSide
import           GunGon.Entity.Types
import           GunGon.Geometry.Convex

mkNGon :: GameConfig -> NGonConfig -> NGon
mkNGon gameConfig nGonConfig =
  mkDummyNGon & size        .~ nGonConfig^.size
              & core        .~ core'
              & energy      .~ core'^.maxEnergy
              & moduleSides .~ fmap (getModuleSide gameConfig) (nGonConfig^.moduleKeys)
  where core' = getNGonCore gameConfig (nGonConfig^.coreKey)

mkDummyNGon :: NGon
mkDummyNGon = NGon
  { _nGonColor       = White
  , _nGonSize        = 0.0
  , _nGonPos         = P $ V2 0 0
  , _nGonRot         = 0.0
  , _nGonVel         = 0.0
  , _nGonEnergy      = 0.0
  , _nGonCore        = mkDummyNGonCore
  , _nGonModuleSides = []
  , _nGonIsDying     = False
  }

getNGonCore :: GameConfig -> String -> NGonCore
getNGonCore gameConfig coreKey_ = maybe mkDummyNGonCore mkNGonCore $ lookup coreKey_ (gameConfig^.coreTypes)

mkNGonCore :: NGonCoreConfig -> NGonCore
mkNGonCore coreConfig =
  mkDummyNGonCore & relSize   .~ coreConfig^.relSize
                  & dRot      .~ coreConfig^.dRot
                  & dSpeed    .~ coreConfig^.dSpeed
                  & maxEnergy .~ coreConfig^.maxEnergy
                  & dEnergy   .~ coreConfig^.dEnergy

mkDummyNGonCore :: NGonCore
mkDummyNGonCore = NGonCore
  { _nGonCoreRelSize   = 0.0
  , _nGonCoreDRot      = 0.0
  , _nGonCoreDSpeed    = 0.0
  , _nGonCoreMaxEnergy = 0.0
  , _nGonCoreDEnergy   = 0.0
  }

updateN :: Int -> NGon -> NGon
updateN _ ng = ng
-- updateN dn ng = ng & ns %~ (max 3 . min 20 . (+dn))
--                    & moduleSides %~ take (ng^.ns + dn) . (<> (repeat $ mkModuleSide NoModule))

getSegments :: (HasModuleSides s2 (f s1), HasSegment s1 a, Functor f) => s2 -> f a
getSegments ng = fmap (^.segment) (ng^.moduleSides)

getFront :: NGon -> DPoint
getFront ng = fst $ (^.segment) (head' $ ng^.moduleSides)
  where
    -- Disgusting hack
    head' [] = mkDummyModuleSide
    head' (x:_) = x

-- getActiveSegments :: NGon -> [DLine]
-- getActiveSegments ng = map (^.nsLine) $ filter (^.nsIsActive) $ ng^.ngSides

getActiveGunSides :: NGon -> [ModuleSide]
getActiveGunSides ng = filter (^.isActive) [side | side <- ng^.moduleSides, is _Gun' (side^.moduleType) ]

nGonToggleWiden :: NGon -> NGon
nGonToggleWiden ng = ng & moduleSides %~ fmap toggleWiden

toggleWiden :: ModuleSide -> ModuleSide
toggleWiden side =
  if side^.isActive then
    side & moduleType . _Gun' . doWiden %~ not
  else
    side

-- nGonResetCooldowns :: NGon -> NGon
-- nGonResetCooldowns ng = ng & moduleSides %~ fmap (moduleType %~ resetModuleCooldown)

-- nGonResetActiveCooldowns :: NGon -> NGon
-- nGonResetActiveCooldowns ng = ng & moduleSides %~ fmap (\side -> if side^.isActive then side & moduleType %~ resetModuleCooldown else side)

resetModuleCooldown :: ModuleType -> ModuleType
resetModuleCooldown (Gun' gun_) = Gun' $ gun_ & cooldown .~ (gun_^.maxCooldown)
resetModuleCooldown moduleType_  = moduleType_

updateSideSegments :: DLine -> DLine -> DLine -> ModuleSide -> ModuleSide
updateSideSegments segment_ segmentOld_ moduleSegment_ =
  (segment .~ segment_) . (segmentOld .~ segmentOld_ ) . (moduleSegment .~ moduleSegment_)

updateSideCooldowns :: Double -> ModuleSide -> ModuleSide
updateSideCooldowns dt side = side & moduleType . _Gun' . cooldown %~ (\t -> max 0 (t - dt))

lineIntersectsNGon :: DLine -> NGon -> Bool
lineIntersectsNGon l ng = not . null $ [s | SSOneIntersect s <- map (intersectSegSeg l) $ getSegments ng]

applyDamage :: Double -> ModuleSide -> ModuleSide
applyDamage damage_ ms = ms & damage %~ (+) ((mitigateShield . mitigateArmor) damage_)
  where mitigateShield = if ms^.isActive then mitigateDamage (ms ^? (moduleType . _Shield')) else id
        mitigateArmor  = mitigateDamage $ ms ^? (moduleType . _Armor')

mitigateDamage :: (Fractional b, HasFraction a b, HasReduction a b) => Maybe a -> b -> b
mitigateDamage (Just module_) damage_ = module_^.fraction * (damage_ - module_^.reduction)
mitigateDamage Nothing        damage_ = damage_
