module GunGon.Entity.ModuleSide where

import           Control.Lens             ((&), (.~), (^.))
import           Data.Map                 (lookup)
import           Prelude                  hiding (lookup)

import           GunGon.Config.GameConfig
import           GunGon.Entity.Types
import           GunGon.Geometry

getModuleSide :: GameConfig -> String -> ModuleSide
getModuleSide gameConfig moduleKey = maybe mkDummyModuleSide mkModuleSide $ lookup moduleKey (gameConfig^.moduleTypes)

mkModuleSide :: ModuleTypeConfig -> ModuleSide
mkModuleSide moduleTypeConfig =
  mkDummyModuleSide & moduleType .~ mkModuleType moduleTypeConfig

mkDummyModuleSide :: ModuleSide
mkDummyModuleSide = ModuleSide
  { _moduleSideSegment       = noline
  , _moduleSideSegmentOld    = noline
  , _moduleSideModuleSegment = noline
  , _moduleSideModuleType    = NoModule'
  , _moduleSideIsActive      = False
  , _moduleSideDamage        = 0.0
  }

mkModuleType :: ModuleTypeConfig -> ModuleType
mkModuleType (ShieldConfig' shieldConfig) = Shield' $ mkShield shieldConfig
mkModuleType (ArmorConfig'  armorConfig)  = Armor'  $ mkArmor  armorConfig
mkModuleType (GunConfig'    gunConfig)    = Gun'    $ mkGun    gunConfig
mkModuleType CollectorConfig'             = Collector'
mkModuleType NoModuleConfig'              = NoModule'

mkShield :: ShieldConfig -> Shield
mkShield shieldConfig =
  mkDummyShield & reduction .~ shieldConfig^.reduction
                & fraction  .~ shieldConfig^.fraction

mkDummyShield :: Shield
mkDummyShield = Shield
  { _shieldReduction = 0.0
  , _shieldFraction  = 0.0
  }

mkArmor :: ArmorConfig -> Armor
mkArmor armorConfig =
  mkDummyArmor & reduction .~ armorConfig^.reduction
               & fraction  .~ armorConfig^.fraction

mkDummyArmor :: Armor
mkDummyArmor = Armor
  { _armorReduction = 0.0
  , _armorFraction  = 0.0
  }

mkGun :: GunConfig -> Gun
mkGun gunConfig =
  mkDummyGun & maxCooldown    .~ gunConfig^.maxCooldown
             & bulletPower    .~ gunConfig^.bulletPower
             & bulletSpeed    .~ gunConfig^.bulletSpeed
             & bulletLifeTime .~ gunConfig^.bulletLifeTime

mkDummyGun :: Gun
mkDummyGun = Gun
  { _gunMaxCooldown    = 0.0
  , _gunBulletPower    = 0.0
  , _gunBulletSpeed    = 0.0
  , _gunBulletLifeTime = 0.0
  , _gunCooldown       = 0.0
  , _gunDoWiden        = False
  }
