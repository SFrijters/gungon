{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TemplateHaskell        #-}

module GunGon.Entity.Types
  ( module GunGon.Entity.Types
  , module GunGon.Types )
where

import           Control.Lens     (makeFields, makePrisms)
import           GHC.Generics

import           GunGon.Data.JSON
import           GunGon.Types

data NGonColor =
    White
  | Magenta
  | Red
  deriving Show

data Bullet = Bullet
  { _bulletLine       :: DLine
  , _bulletLineOld    :: DLine
  , _bulletDir        :: DVec
  , _bulletSpeed      :: Double
  , _bulletDSizeLeft  :: Double
  , _bulletDSizeRight :: Double
  , _bulletPower      :: Double
  , _bulletTime       :: Double
  , _bulletLifeTime   :: Double
  } deriving Show
makeFields ''Bullet

data Gun = Gun
  { _gunMaxCooldown    :: Double
  , _gunBulletPower    :: Double
  , _gunBulletSpeed    :: Double
  , _gunBulletLifeTime :: Double
  , _gunDoWiden        :: Bool
  , _gunCooldown       :: Double
  } deriving Show
makeFields ''Gun

data GunConfig = GunConfig
  { _gunConfigMaxCooldown    :: Double
  , _gunConfigBulletPower    :: Double
  , _gunConfigBulletSpeed    :: Double
  , _gunConfigBulletLifeTime :: Double
  } deriving (Generic, Show)
makeFields ''GunConfig

instance ToJSON GunConfig where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_gunConfig" }

instance FromJSON GunConfig where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_gunConfig" }

data Shield = Shield
  { _shieldReduction :: Double
  , _shieldFraction  :: Double
  } deriving Show
makeFields ''Shield

data ShieldConfig = ShieldConfig
  { _shieldConfigReduction :: Double
  , _shieldConfigFraction  :: Double
  } deriving (Generic, Show)
makeFields ''ShieldConfig

instance ToJSON ShieldConfig where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_shieldConfig" }

instance FromJSON ShieldConfig where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_shieldConfig" }

data Armor = Armor
  { _armorReduction :: Double
  , _armorFraction  :: Double
  } deriving Show
makeFields ''Armor

data ArmorConfig = ArmorConfig
  { _armorConfigReduction :: Double
  , _armorConfigFraction  :: Double
  } deriving (Generic, Show)
makeFields ''ArmorConfig

instance ToJSON ArmorConfig where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_armorConfig" }

instance FromJSON ArmorConfig where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_armorConfig" }

data ModuleType =
    Shield' Shield
  | Gun' Gun
  | Armor' Armor
  | Collector'
  | NoModule'
  deriving Show
makePrisms ''ModuleType

data ModuleTypeConfig =
    ShieldConfig' ShieldConfig
  | GunConfig' GunConfig
  | ArmorConfig' ArmorConfig
  | CollectorConfig'
  | NoModuleConfig'
  deriving (Generic, Show)
makePrisms ''ModuleTypeConfig

instance ToJSON ModuleTypeConfig where
  toJSON = genericToJSON defaultOptions { constructorTagModifier = configConstructorTagModifier }

instance FromJSON ModuleTypeConfig where
  parseJSON = genericParseJSON defaultOptions { constructorTagModifier = configConstructorTagModifier }

data ModuleSide = ModuleSide
  { _moduleSideSegment       :: DLine
  , _moduleSideSegmentOld    :: DLine
  , _moduleSideModuleSegment :: DLine
  , _moduleSideModuleType    :: ModuleType
  , _moduleSideIsActive      :: Bool
  , _moduleSideDamage        :: Double
  } deriving Show
makeFields ''ModuleSide

data NGonCore = NGonCore
  { _nGonCoreRelSize   :: Double
  , _nGonCoreDRot      :: Double
  , _nGonCoreDSpeed    :: Double
  , _nGonCoreMaxEnergy :: Double
  , _nGonCoreDEnergy   :: Double
  } deriving Show
makeFields ''NGonCore

data NGonCoreConfig = NGonCoreConfig
  { _nGonCoreConfigRelSize   :: Double
  , _nGonCoreConfigDRot      :: Double
  , _nGonCoreConfigDSpeed    :: Double
  , _nGonCoreConfigMaxEnergy :: Double
  , _nGonCoreConfigDEnergy   :: Double
  } deriving (Generic, Show)
makeFields ''NGonCoreConfig

instance ToJSON NGonCoreConfig where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_nGonCoreConfig" }

instance FromJSON NGonCoreConfig where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_nGonCoreConfig" }

data NGon = NGon
  { _nGonSize        :: Double
  , _nGonPos         :: DPoint
  , _nGonRot         :: Double
  , _nGonVel         :: DVec
  , _nGonColor       :: NGonColor
  , _nGonEnergy      :: Double
  , _nGonCore        :: NGonCore
  , _nGonModuleSides :: [ModuleSide]
  , _nGonIsDying     :: Bool
  } deriving Show
makeFields ''NGon

data NGonConfig = NGonConfig
  { _nGonConfigSize       :: Double
  , _nGonConfigCoreKey    :: String
  , _nGonConfigModuleKeys :: [String]
  } deriving (Generic, Show)
makeFields ''NGonConfig

instance ToJSON NGonConfig where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_nGonConfig" }

instance FromJSON NGonConfig where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_nGonConfig" }

newtype Player = Player
  { _playerNGon :: NGon
  } deriving Show
makeFields ''Player

newtype PlayerConfig = PlayerConfig
  { _playerConfigNGon :: NGonConfig
  } deriving (Generic, Show)
makeFields ''PlayerConfig

instance ToJSON PlayerConfig where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_playerConfig" }

instance FromJSON PlayerConfig where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_playerConfig" }

newtype Enemy = Enemy
  { _enemyNGon :: NGon
  } deriving Show
makeFields ''Enemy

newtype EnemyConfig = EnemyConfig
  { _enemyConfigNGon :: NGonConfig
  } deriving (Generic, Show)
makeFields ''EnemyConfig

instance ToJSON EnemyConfig where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_enemyConfig" }

instance FromJSON EnemyConfig where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_enemyConfig" }

data Corpse = Corpse
  { _corpseCorePos       :: DPoint
  , _corpseCoreEnergy    :: Double
  , _corpseCoreSize      :: Double
  , _corpseCoreLifeTime  :: Double
  , _corpseSegments      :: [DLine]
  , _corpseSegVelocities :: [DVec]
  , _corpseSegRotations  :: [Double]
  , _corpseSegLifeTime   :: Double
  , _corpseTime          :: Double
  } deriving Show
makeFields ''Corpse

data CorpseConfig = CorpseConfig
  { _corpseConfigCoreSize       :: Double
  , _corpseConfigCoreLifeTime   :: Double
  , _corpseConfigSegLifeTime    :: Double
  , _corpseConfigEnergyFraction :: Double
  } deriving (Generic, Show)
makeFields ''CorpseConfig

instance ToJSON CorpseConfig where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_corpseConfig" }

instance FromJSON CorpseConfig where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_corpseConfig" }

newtype Terrain = Terrain
  { _terrainModuleSides :: [ModuleSide]
  } deriving Show
makeFields ''Terrain

data TerrainConfig = TerrainConfig
  { _terrainConfigCoordinates :: [(Double, Double)]
  , _terrainConfigModuleKeys  :: [String]
  } deriving (Generic, Show)
makeFields ''TerrainConfig

instance ToJSON TerrainConfig where
  toJSON = genericToJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_terrainConfig" }

instance FromJSON TerrainConfig where
  parseJSON = genericParseJSON defaultOptions { fieldLabelModifier = configFieldLabelModifier "_terrainConfig" }
