module GunGon.Entity.Enemy where

import           Control.Lens             ((%~), (&), (.~), (^.))
import           Data.Map                 (lookup)
import           Prelude                  hiding (lookup)

import           GunGon.Config.GameConfig
import           GunGon.Entity.Bullet
import           GunGon.Entity.NGon
import           GunGon.Entity.Types

spawnEnemy :: GameConfig -> String -> DPoint -> Enemy
spawnEnemy gameConfig_ enemyKey_ pos_ = getEnemy gameConfig_ enemyKey_ & nGon . pos   .~ pos_
                                                                       & nGon . color .~ White
getEnemy :: GameConfig -> String -> Enemy
getEnemy gameConfig enemyKey = maybe mkDummyEnemy (mkEnemy gameConfig) $ lookup enemyKey (gameConfig^.enemyTypes)

mkEnemy :: GameConfig -> EnemyConfig -> Enemy
mkEnemy gameConfig_ enemyConfig_ = mkDummyEnemy & nGon .~ mkNGon gameConfig_ (enemyConfig_^.nGon)

mkDummyEnemy :: Enemy
mkDummyEnemy = Enemy { _enemyNGon = mkDummyNGon }

spawnInitialEnemies :: GameConfig -> [Enemy]
spawnInitialEnemies gameConfig_ = spawn <$> gameConfig_^.initialEnemies
  where spawn (enemyKey, (x, y)) = spawnEnemy gameConfig_ enemyKey $ P $ V2 x y

updateEnemyIsDying :: Enemy -> Enemy
updateEnemyIsDying en = en & nGon . isDying %~ (||) (en^.nGon.energy < 0)

hitBulletsEnemies :: ([Bullet], [Enemy]) -> ([Bullet], [Enemy])
hitBulletsEnemies (bs, ens) = (bs', ens')
  where
    ngs = fmap (^.nGon) ens
    (bs', ngs') = hitBulletsNGons (bs, ngs)
    ens' = zipWith (nGon .~) ngs' ens
