module GunGon.Entity.Terrain where

import           Control.Lens             ((&), (.~), (^.))
import           Data.Map                 (lookup)
import           Prelude                  hiding (lookup)

import           GunGon.Config.GameConfig
import           GunGon.Entity.ModuleSide
import           GunGon.Entity.Types
import           GunGon.Geometry

spawnTerrain :: GameConfig -> TerrainConfig -> Terrain
spawnTerrain gameConfig_ terrainConfig_ = Terrain $ zipWith3 mkTerrainSide moduleSides' segments' moduleSegments'
  where segments'       = closedSegs $ fmap (P . uncurry V2) (terrainConfig_^.coordinates)
        shift'          = 5.0
        moduleSegments' = fmap (segTranslatePerp (-shift')) segments' -- TODO: Fix intersections / overlap
        moduleSides'    = fmap (\moduleKey -> maybe mkDummyModuleSide mkModuleSide $ lookup moduleKey (gameConfig_^.moduleTypes)) (terrainConfig_^.moduleKeys)

mkTerrainSide :: ModuleSide -> DLine -> DLine -> ModuleSide
mkTerrainSide moduleSide_ segment_ moduleSegment_ =
  moduleSide_ & segment       .~ segment_
              & moduleSegment .~ moduleSegment_
