{-# LANGUAGE FlexibleContexts #-}

module GunGon.Entity.Bullet where

import           Control.Lens           ((%~), (&), (.~), (^.), (^..), (^?))
import           Control.Lens.Extras    (is)
import           Linear                 (dot, perp, (*^))

import           GunGon.Entity.NGon
import           GunGon.Entity.Types
import           GunGon.Geometry
import           GunGon.Geometry.Convex

mkBullet :: DPoint -> DVec -> Double -> Double -> Double -> Double -> Double -> Bullet
mkBullet pos_ dir_ speed_ width_ dWidth_ power_ lifeTime_ = Bullet
  { _bulletLine       = line'
  , _bulletLineOld    = line'
  , _bulletDir        = dir_
  , _bulletSpeed      = speed_
  , _bulletDSizeLeft  = dWidth_
  , _bulletDSizeRight = dWidth_
  , _bulletPower      = power_
  , _bulletTime       = 0.0
  , _bulletLifeTime   = lifeTime_
  }
  where
    line' = (pos_ + (P $ width_ *^ perp dir_), pos_ - (P $ width_ *^ perp dir_))

powerDensity :: Bullet -> Double
powerDensity b = (b^.power) / segLen (b^.line)

nGonFireBullets :: NGon -> (NGon, [Bullet])
nGonFireBullets ng = if canFire then (ng', firedBullets ng) else (ng, [])
  where activeSides     = [ side^.moduleType | side <- ng^.moduleSides, side^.isActive ]
        neededEnergy    = sum $ fmap (^.bulletPower) (activeSides ^.. traverse . _Gun')
        hasNoCooldown   = all (<=0) $ (^.cooldown) <$> (activeSides ^.. traverse . _Gun')
        canFire         = hasNoCooldown && ng^.energy >= neededEnergy && neededEnergy > 0
        ng'             = ng & energy      %~ (\e -> e - neededEnergy)
                             & moduleSides %~ fmap (\side -> if side^.isActive && is _Gun' (side^.moduleType) then side & moduleType %~ resetModuleCooldown else side)

firedBullets :: NGon -> [Bullet]
firedBullets ng = fmap mkBullet' (getActiveGunSides ng)
  where
    mkBullet' side = mkBullet pos' dir' speed' width' dWidth' power' lifeTime'
      where
        seg = side^.segment
        dir' = segPerp seg
        pos' = segMid seg + P dir'
        speed' = maybe 0 (^.bulletSpeed) (side^?moduleType . _Gun') + dot dir' (ng^.vel)
        width' = 0.5 * segLen seg
        doWiden' = maybe False (^.doWiden) (side^?moduleType . _Gun')
        dWidth' = if doWiden' then 0.25 * speed' * segLen seg / segLen (segMid seg, ng^.pos) else 0.0
        power' = maybe 0 (^.bulletPower) (side^?moduleType . _Gun')
        lifeTime' = maybe 0 (^.bulletLifeTime) (side^?moduleType . _Gun')

hitBulletsNGons :: ([Bullet], [NGon]) -> ([Bullet], [NGon])
hitBulletsNGons x@(_, []) = x
hitBulletsNGons x@([], _) = x
hitBulletsNGons (bs, ngs) = hitBulletsNGons' bs ngs []

hitBulletsNGons' :: [Bullet] -> [NGon] -> [NGon] -> ([Bullet], [NGon])
hitBulletsNGons' bs []       ngAfter = (bs, ngAfter)
hitBulletsNGons' bs (ng:ngs) ngAfter = hitBulletsNGons' bs' ngs (ng':ngAfter)
  where (bs', ng') = hitBulletsNGon (bs, ng)

hitBulletsNGon :: ([Bullet], NGon) -> ([Bullet], NGon)
hitBulletsNGon (bs, ng) = foldr hitBulletNGon initial bs
  where initial = ([], ng)

hitBulletNGon :: Bullet -> ([Bullet], NGon) -> ([Bullet], NGon)
hitBulletNGon b (bs, ng) =
  case intersection of
    -- Either there are two intersections, in which case we split the bullet
    -- into three parts, keep the first and last ones and use the middle one to
    -- damage the NGon.
    SCTwoIntersect p1 _ p2 _ ->
      -- trace ("Found two intersections, split into" ++
      --        "\nb1 = " ++ show b1 ++ "\nb2 = " ++ show b2 ++
      --        "\ndamaged = " ++ show (dmg (p1,p2)) ++ "\n")
      (b1:b2:bs, ng')
      where
        l1  = (bp1, p1)
        l2  = (p2, bp2)
        b1  = splitBullet l1 True
        b2  = splitBullet l2 False
        ng' = ng & energy %~ (\e -> e - dmg (p1, p2))
    -- Or there is exactly one intersection, in which case we find out which part
    -- of the bullet is outside, keep that part and use the other to damage the NGon.
    SCOneIntersect p1 nl ->
      -- trace ("Found one intersection, split into" ++
      --        "\nb1 = " ++ (if firstOut then show b1 else show b2) ++
      --        "\ndamaged = " ++ (if firstOut then show (dmg (p1, bp2)) else show (dmg (bp1, p1)))) $
      if firstOut then (b1:bs, ng1) else (b2:bs, ng2)
      where
        firstOut = dot (segVec l1) (segPerp nl) < 0
        l1  = (bp1, p1)
        l2  = (p1, bp2)
        b1  = splitBullet l1 True
        ng1 = ng & energy %~ (\e -> e - dmg l2)
        b2  = splitBullet l2 False
        ng2 = ng & energy %~ (\e -> e - dmg l1)
    -- Or there are zero intersections, in which we check if the trajectory of the bullet
    -- since the previous time step intersected the NGon.
    SCNoIntersect ->
      -- trace ("Found no intersection" ++
      --        if inside then "\ndamaged = " ++ show (dmg (b^.bLine)) else "\nkeeping = " ++ show (b^.bPower)) $
      if inside then (bs, ng') else (b:bs, ng)
      where
        inside  = inside1 || inside2
        mid1    = (fst bl, ng^.pos)
        mid2    = (snd bl, ng^.pos)
        inside1 = lineIntersectsNGon step1 ng && not (lineIntersectsNGon mid1 ng)
        inside2 = lineIntersectsNGon step2 ng && not (lineIntersectsNGon mid2 ng)
        ng'     = ng & energy %~ (\e -> e - dmg (b^.line))
  where
    bl@(bp1, bp2)   = b^.line
    intersection    = intersectSegSegs bl $ getSegments ng
    step1           = (fst (b^.lineOld), fst bl)
    step2           = (snd (b^.lineOld), snd bl)
    dmg l           = segLen l * powerDensity b
    splitBullet l e =
      b & line       .~ l
        & lineOld    .~ segTranslate ((-1) *^ segVec step1) l -- step1 and step2 are the same after segVec
        & dSizeLeft  %~ (if e then id else const 0)
        & dSizeRight %~ (if e then const 0 else id)
        & power      %~ (*) (segLen l / segLen (b^.line))

