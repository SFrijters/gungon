{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ViewPatterns     #-}

module GunGon.State.SceneState where

import qualified SDL

import           Control.Lens             ((%%~), (&), (.~), (^.))
import           Control.Monad.Reader     (MonadReader, ask)

import           GunGon.Config.GameConfig
import           GunGon.State.ActionState
import           GunGon.State.GameState
import           GunGon.State.Types

mkInitialSceneState :: GameConfig -> DVec -> SceneState
mkInitialSceneState gameConfig windowSize_ = SceneState
  { _sceneStateScene      = SceneTitle
  , _sceneStateNextScene  = SceneTitle
  , _sceneStateGameState  = gs
  , _sceneStateMousePos   = P $ V2 0 0
  , _sceneStateWindowSize = windowSize_
  , _sceneStateDoToggleWM = False
  }
  where gs = mkInitialGameState gameConfig

ssStep :: (MonadReader GameConfig m) => InputState -> DVec -> SceneState -> m SceneState
ssStep inputState_ windowSize_ ss = ssStep' inputState_ ss'
  where ss' = ss & scene      .~ sceneTransition inputState_ ss
                 & mousePos   .~ inputState_^.mousePos
                 & windowSize .~ windowSize_
                 & doToggleWM .~ getToggleWM inputState_

sceneTransition :: InputState -> SceneState -> Scene
sceneTransition inputState_ sceneState_
  | sceneState_^.scene == SceneGame &&
    gsIsGameOver (sceneState_^.gameState) = SceneGameOver
  | sceneState_^.scene == SceneGameStart  = SceneGame
  | otherwise                             = foldr getSceneNextScene (sceneState_^.scene) (inputState_^.keysPressed)

ssStep' :: (MonadReader GameConfig m) => InputState -> SceneState -> m SceneState
ssStep' inputState_ ss = do
  gameConfig <- ask
  case ss^.scene of
    SceneGame      -> ss & gameState %%~ gsStep (getActionState inputState_) (ss^.windowSize) (realToFrac $ inputState_^.diffTime)
    SceneGameOver  -> ss & gameState %%~ gsStep (mkEmptyActionState inputState_) (ss^.windowSize) (realToFrac $ inputState_^.diffTime)
    SceneGameStart -> return $ ss & gameState .~ mkInitialGameState gameConfig
    _              -> return ss

getSceneNextScene :: SDL.Keysym -> Scene -> Scene
getSceneNextScene (SDL.keysymKeycode -> SDL.KeycodeSpace ) SceneTitle    = SceneGameStart
getSceneNextScene (SDL.keysymKeycode -> SDL.KeycodeSpace ) SceneGameOver = SceneGameStart
getSceneNextScene (SDL.keysymKeycode -> SDL.KeycodeQ     ) _             = SceneQuit
getSceneNextScene (SDL.keysymKeycode -> SDL.KeycodeEscape) SceneGame     = ScenePause
getSceneNextScene (SDL.keysymKeycode -> SDL.KeycodeEscape) ScenePause    = SceneGame
getSceneNextScene _                                        s             = s

getToggleWM :: InputState -> Bool
getToggleWM inputState_ = or $ fmap (\k -> SDL.keysymKeycode k == SDL.KeycodeF) (inputState_^.keysPressed)
