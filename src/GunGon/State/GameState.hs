{-# LANGUAGE FlexibleContexts #-}

module GunGon.State.GameState where

import           Control.Arrow            ((>>>))
import           Control.Lens             ((%~), (&), (.~), (^.))
import           Control.Monad.Reader     (MonadReader, ask)
import           System.Random            (mkStdGen, randomR, split)

import           GunGon.Config.GameConfig
import           GunGon.Entity
import           GunGon.Entity.Bullet
import           GunGon.Entity.Corpse
import           GunGon.Entity.Enemy
import           GunGon.Entity.NGon
import           GunGon.Entity.Player
import           GunGon.Entity.Terrain
import           GunGon.Entity.Types

import           GunGon.State.Types

mkInitialGameState :: GameConfig -> GameState
mkInitialGameState gameConfig = GameState
  { _gameStatePlayer       = spawnPlayer gameConfig "basic" $ P $ V2 0 0
  , _gameStatePlayerIsDead = False
  , _gameStateEnemies      = spawnInitialEnemies gameConfig
  , _gameStateBullets      = []
  , _gameStateCorpses      = []
  , _gameStateTerrain      = fmap (spawnTerrain gameConfig) (gameConfig^.terrainConfig)
  , _gameStateStdGen       = mkStdGen $ gameConfig^.randomSeed
  , _gameStateMousePosRel  = P $ V2 0 0
  }

-- Step the game state based on the action state and time step
gsStep :: (MonadReader GameConfig m) => ActionState -> DVec -> TimeStep -> GameState -> m GameState
gsStep as size_ dt gs = do
  gameConfig <- ask
  return $ (gsUpdateMousePosRel (as^.mousePos) size_
           >>> gsUpdateActiveSides
           >>> gsApplyActions (as^.actions) dt
           >>> gsIntegrate dt
           >>> gsIntersectBullets
           >>> gsSpawn gameConfig dt
           >>> gsFilterExpired) gs

gsIsGameOver :: GameState -> Bool
gsIsGameOver gs = gs^.player.nGon.isDying

gsUpdateMousePosRel :: DPoint -> DVec -> GameState -> GameState
gsUpdateMousePosRel mousePos_ size_ gameState_ = gameState_ & mousePosRel .~ mousePosRel'
  where mousePosRel' = mousePos_ - (P $ fmap (/2) size_) + gameState_^.player.nGon.pos

-- Set the active segments on the player NGon
gsUpdateActiveSides :: GameState -> GameState
gsUpdateActiveSides gs = gs & player . nGon %~ updatePlayerActiveSides (gs^.mousePosRel)

-- Apply a list of actions to a game state using a time step if needed
gsApplyActions :: [GameAction] -> TimeStep -> GameState -> GameState
gsApplyActions as dt gs = foldr gsApplyAction' gs as
  where gsApplyAction' = flip gsApplyAction dt :: GameAction -> GameState -> GameState

-- Apply a single action to a game state using a time step if needed
gsApplyAction :: GameAction -> TimeStep -> GameState -> GameState
gsApplyAction Brake            _  gs = gs & player . nGon . vel .~ 0.0
gsApplyAction RotateLeft       dt gs = gs & player %~ rotate (-dt)
gsApplyAction RotateRight      dt gs = gs & player %~ rotate dt
gsApplyAction IncreaseN        _  gs = gs & player . nGon %~ updateN 1
gsApplyAction DecreaseN        _  gs = gs & player . nGon %~ updateN (-1)
gsApplyAction IncreaseV        dt gs = gs & player %~ accelerate dt
gsApplyAction DecreaseV        dt gs = gs & player %~ accelerate (-dt)
gsApplyAction FireBullet       _  gs = gs & gsFireBullets
gsApplyAction ToggleWiden      _  gs = gs & player . nGon %~ nGonToggleWiden
gsApplyAction Noop             _  gs = gs

-- Update a game state by firing bullets
gsFireBullets :: GameState -> GameState
gsFireBullets gs = gs & bullets %~ (<> bullets')
                      & player . nGon .~ ng'
  where (ng', bullets') = nGonFireBullets $ gs^.player.nGon

-- Integrate all things that need to be integrated
gsIntegrate :: TimeStep -> GameState -> GameState
gsIntegrate dt = gsIntegratePlayer dt
             >>> gsIntegrateBullets dt
             >>> gsIntegrateEnemies dt
             >>> gsIntegrateCorpses dt

gsIntegratePlayer :: TimeStep -> GameState -> GameState
gsIntegratePlayer dt gs = gs & player %~ integrate dt

gsIntegrateBullets :: TimeStep -> GameState -> GameState
gsIntegrateBullets dt gs = gs & bullets %~ fmap (integrate dt)

gsIntegrateEnemies :: TimeStep -> GameState -> GameState
gsIntegrateEnemies dt gs = gs & enemies %~ fmap (integrate dt)

gsIntegrateCorpses :: TimeStep -> GameState -> GameState
gsIntegrateCorpses dt gs = gs & corpses %~ fmap (removeCorpseDeadSegments . integrate dt)

-- Intersect bullets with stuff
gsIntersectBullets :: GameState -> GameState
gsIntersectBullets = gsIntersectBulletsEnemies
                 >>> gsIntersectBulletsPlayer
                 -- >>> gsIntersectBulletsTerrain

gsIntersectBulletsEnemies :: GameState -> GameState
gsIntersectBulletsEnemies gs = gs & enemies .~ fmap updateEnemyIsDying es'
                                  & bullets .~ bs'
  where (bs', es') = hitBulletsEnemies (gs^.bullets, gs^.enemies)

gsIntersectBulletsPlayer :: GameState -> GameState
gsIntersectBulletsPlayer gs = gs & player  .~ updatePlayerIsDying p'
                                 & bullets .~ bs'
  where (bs', p') = hitBulletsPlayer (gs^.bullets, gs^.player)

-- gsIntersectBulletsTerrain :: GameState -> GameState
-- gsIntersectBulletsTerrain gs = gs & bullets .~ bs'
--   where (bs', _) = hitBulletsTerrains (gs^.bullets, gs^.terrain)

-- Spawn new entities
gsSpawn :: GameConfig -> TimeStep -> GameState -> GameState
gsSpawn gameConfig dt = gsSpawnEnemies gameConfig dt
                    >>> gsSpawnCorpses gameConfig

-- TODO: Make this into something more sensible
gsSpawnEnemies :: GameConfig -> TimeStep -> GameState -> GameState
gsSpawnEnemies gameConfig dt gs = gs & enemies %~ (<>) ngons'
                                     & stdGen  .~ rng'
  where (rng, rng') = split (gs^.stdGen)
        (c,x,y) = fst $ randomR ((0, 0, 0), (500, 500, 1)) rng
        ngons' = [ spawnEnemy gameConfig "n3" (P $ V2 x y) | c * dt < gameConfig^.spawnChance ]

-- Spawn NGon corpses
gsSpawnCorpses :: GameConfig -> GameState -> GameState
gsSpawnCorpses gameConfig gs = gs & corpses %~ updateDead gameConfig rng (((^.nGon) <$> dying) <> playerCorpses)
                                  & stdGen  .~ rng'
  where dying = filter (^.nGon.isDying) (gs^.enemies)
        (rng, rng') = split (gs^.stdGen)
        playerCorpses = [ gs^.player.nGon | gs^.player.nGon.isDying, not $ gs^.playerIsDead ]

gsFilterExpired :: GameState -> GameState
gsFilterExpired gs = gs & bullets %~ filter (not . isExpired)
                        & corpses %~ filter (not . isExpired)
                        & enemies %~ filter (not . isExpired)
                        & playerIsDead .~ isExpired (gs^.player)
