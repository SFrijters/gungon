module GunGon.State.ActionState where

import           Control.Lens       ((^.))
import           Data.List.Unique   (sortUniq)
import qualified SDL

import           GunGon.State.Types

mkEmptyActionState :: InputState -> ActionState
mkEmptyActionState inputState_ = ActionState
  { _actionStateActions  = []
  , _actionStateMousePos = inputState_^.mousePos }

getActionState :: InputState -> ActionState
getActionState inputState_ = ActionState
  { _actionStateActions  = sortUniq $ filter (/= Noop) (keyHeldActions ++ keyPressedActions ++ mouseHeldActions)
  , _actionStateMousePos = inputState_^.mousePos }
  where keyHeldActions    = fmap getKeyHeldAction    $ inputState_^.keysHeld
        keyPressedActions = fmap getKeyPressedAction $ inputState_^.keysPressed
        mouseHeldActions  = fmap getMouseHeldAction  $ inputState_^.mouseHeld

getKeyHeldAction :: SDL.Keysym -> GameAction
getKeyHeldAction k =
  case SDL.keysymKeycode k of
    SDL.KeycodeLeft  -> RotateLeft
    SDL.KeycodeA     -> RotateLeft
    SDL.KeycodeRight -> RotateRight
    SDL.KeycodeD     -> RotateRight
    SDL.KeycodeUp    -> IncreaseV
    SDL.KeycodeW     -> IncreaseV
    SDL.KeycodeDown  -> DecreaseV
    SDL.KeycodeS     -> DecreaseV
    SDL.KeycodeSpace -> FireBullet
    _                -> Noop

getKeyPressedAction :: SDL.Keysym -> GameAction
getKeyPressedAction k =
  case SDL.keysymKeycode k of
    SDL.KeycodeB       -> Brake
    SDL.KeycodeG       -> ToggleWiden
    SDL.KeycodeKPPlus  -> IncreaseN
    SDL.KeycodeKPMinus -> DecreaseN
    _                  -> Noop

getMouseHeldAction :: SDL.MouseButton -> GameAction
getMouseHeldAction m =
  case m of
    SDL.ButtonLeft -> FireBullet
    _              -> Noop
