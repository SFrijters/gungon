{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TemplateHaskell        #-}

module GunGon.State.Types
  ( module GunGon.State.Types
  , module GunGon.Types ) where

import qualified SDL

import           Control.Lens        (makeFields, (^.))
import           Data.Char           (chr)
import           Data.List           (intercalate)
import           Data.Time.Clock     (NominalDiffTime, UTCTime)
import           System.Random       (StdGen)

import           GunGon.Entity.Types (Bullet, Corpse, Enemy, Player, Terrain)
import           GunGon.Types

data EventState = EventState
  { _eventStateEvents   :: [SDL.Event]
  , _eventStateMousePos :: MousePos
  } deriving Show
makeFields ''EventState

data InputState = InputState
  { _inputStateKeysHeld    :: [SDL.Keysym]
  , _inputStateKeysPressed :: [SDL.Keysym]
  , _inputStateMouseHeld   :: [SDL.MouseButton]
  , _inputStateMousePos    :: MousePos
  , _inputStateCurrentTime :: UTCTime
  , _inputStateDiffTime    :: NominalDiffTime
  }
makeFields ''InputState

data GameAction =
    Noop
  | Brake
  | ToggleWiden
  | RotateLeft
  | RotateRight
  | IncreaseV
  | DecreaseV
  | IncreaseN
  | DecreaseN
  | FireBullet
  deriving (Ord, Eq, Show)

data ActionState = ActionState
  { _actionStateActions  :: [GameAction]
  , _actionStateMousePos :: MousePos
  } deriving Show
makeFields ''ActionState

data GameState = GameState
  { _gameStatePlayer       :: Player
  , _gameStatePlayerIsDead :: Bool
  , _gameStateBullets      :: [Bullet]
  , _gameStateEnemies      :: [Enemy]
  , _gameStateCorpses      :: [Corpse]
  , _gameStateTerrain      :: [Terrain]
  , _gameStateStdGen       :: StdGen
  , _gameStateMousePosRel  :: DPoint
  } deriving Show
makeFields ''GameState

data Scene =
    SceneTitle
  | SceneMenu
  | SceneGame
  | SceneGameOver
  | SceneGameStart
  | ScenePause
  | SceneQuit
  deriving (Show, Eq)

data SceneState = SceneState
  { _sceneStateScene      :: Scene
  , _sceneStateNextScene  :: Scene
  , _sceneStateGameState  :: GameState
  , _sceneStateMousePos   :: DPoint
  , _sceneStateWindowSize :: DVec
  , _sceneStateDoToggleWM :: Bool
  } deriving Show
makeFields ''SceneState

instance Show InputState where
  show inputState' = mconcat ["keys held = ", keysHeld', " keys pressed = ", keysPressed', " mouse held = ", mouseHeld']
    where
      keysHeld'    = intercalate ", " $ fmap ((:[]). keyChar) $ inputState'^.keysHeld
      keysPressed' = intercalate ", " $ fmap ((:[]). keyChar) $ inputState'^.keysPressed
      mouseHeld'   = intercalate ", " $ fmap show             $ inputState'^.mouseHeld
      keyChar ksym = chr $ fromIntegral $ toInteger $ SDL.unwrapKeycode $ SDL.keysymKeycode ksym
