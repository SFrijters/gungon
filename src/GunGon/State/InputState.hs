{-# LANGUAGE FlexibleContexts #-}

module GunGon.State.InputState where

import           Control.Lens           ((%~), (&), (.~), (^.))
import           Control.Monad.IO.Class (MonadIO (..))
import           Data.List.Unique       (sortUniq)
import           Data.Time.Clock        (NominalDiffTime, UTCTime, diffUTCTime,
                                         getCurrentTime)
import qualified SDL

import           GunGon.State.Types

pollEvents :: MonadIO m => m EventState
pollEvents = do
  events' <- SDL.pollEvents
  mousePos' <- dPoint <$> SDL.getAbsoluteMouseLocation
  return EventState
    { _eventStateEvents   = events'
    , _eventStateMousePos = mousePos'
    }

mkInitialInputState :: UTCTime -> InputState
mkInitialInputState t = InputState
  { _inputStateKeysHeld    = []
  , _inputStateKeysPressed = []
  , _inputStateMouseHeld   = []
  , _inputStateMousePos    = P $ V2 0 0
  , _inputStateCurrentTime = t
  , _inputStateDiffTime    = diffUTCTime t t
  }

updateTime :: MonadIO m => UTCTime -> m (NominalDiffTime, UTCTime)
updateTime t = do
  t' <- liftIO getCurrentTime
  let dt = diffUTCTime t' t
  dt `seq` return (dt, t')

updateInputState :: MonadIO m => InputState -> EventState -> m InputState
updateInputState inputState_ eventState_ = do
  (dt, t') <- updateTime (inputState_^.currentTime)
  let clearedInputState = inputState_ & keysPressed .~ []
                                      & mousePos    .~ eventState_^.mousePos
                                      & currentTime .~ t'
                                      & diffTime    .~ dt
  -- The fold direction seems to matter here!
  return $ foldl go clearedInputState (eventState_^.events)
  where
    go inputState' ev =
      case SDL.eventPayload ev of
        SDL.KeyboardEvent ed ->
          case SDL.keyboardEventKeyMotion ed of
            SDL.Pressed ->
              if SDL.keyboardEventRepeat ed then
                inputState' & keysHeld .~ sortUniq (k:khs)
              else
                inputState' & keysHeld .~ sortUniq (k:khs)
                            & keysPressed .~ sortUniq (k:kps)
            SDL.Released -> inputState' & keysHeld %~ filter (/= k)
          where k = SDL.keyboardEventKeysym ed
                khs = inputState'^.keysHeld
                kps = inputState'^.keysPressed
        SDL.MouseButtonEvent ed ->
          case SDL.mouseButtonEventMotion ed of
            SDL.Pressed  -> inputState' & mouseHeld .~ sortUniq (m:ms)
            SDL.Released -> inputState' & mouseHeld %~ filter (/= m)
          where m = SDL.mouseButtonEventButton ed
                ms = inputState'^.mouseHeld
        _ -> inputState'
