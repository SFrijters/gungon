module GunGon.Geometry.Convex where

import           Linear          (crossZ, dot, (*^))

import           GunGon.Geometry
import           GunGon.Types

data IntersectSegConvex
  = SCNoIntersect
  | SCOneIntersect DPoint DLine
  | SCTwoIntersect DPoint DLine DPoint DLine
  deriving Show

data IntersectSegSeg
  = SSNoIntersect
  | SSOneIntersect DPoint
  deriving Show

triangleSegs :: DTriangle -> [DLine]
triangleSegs (p1, p2, p3) = [(p1, p2), (p2, p3), (p3, p1)]

intersectSegTriangle :: DLine -> DTriangle -> IntersectSegConvex
intersectSegTriangle l t = intersectSegSegs l $ triangleSegs t

pointInsideTriangle :: DPoint -> DTriangle -> Bool
pointInsideTriangle p0 (p1, p2, p3) =
  (abs d23 > 1e-20) && a > 0 && b > 0 && a + b < 1
  where
    v2 = p2 - p1
    v3 = p3 - p1
    d02 = det p0 v2
    d03 = det p0 v3
    d12 = det p1 v2
    d13 = det p1 v3
    d23 = det v2 v3
    a =  (d03 - d13) / d23
    b = -(d02 - d12) / d23
    det (P (V2 x1 y1)) (P (V2 x2 y2)) = x1 * y2 - x2 * y1

quadrangleSegs :: DQuadrangle -> [DLine]
quadrangleSegs (p1, p2, p3, p4) = [(p1, p2), (p2, p3), (p3, p4), (p4, p1)]

intersectSegQuadrangle :: DLine -> DQuadrangle -> IntersectSegConvex
intersectSegQuadrangle l q = intersectSegSegs l $ quadrangleSegs q

pointInsideQuadrangle :: DPoint -> DQuadrangle -> Bool
pointInsideQuadrangle p (p1, p2, p3, p4) =
  pointInsideTriangle p t1 || pointInsideTriangle p t2
  where
    t1 = (p1, p2, p3)
    t2 = (p1, p3, p4)

intersectSegSeg :: DLine -> DLine -> IntersectSegSeg
intersectSegSeg (P (V2 p1x p1y), P (V2 p2x p2y)) (P (V2 q1x q1y), P (V2 q2x q2y)) =
  if s >= 0 && s <= 1 && t >= 0 && t <= 1 && (abs det > 1e-20) then
    SSOneIntersect intersection
  else
    SSNoIntersect
  where
    s1x = p2x - p1x
    s1y = p2y - p1y
    s2x = q2x - q1x
    s2y = q2y - q1y
    det = -s2x * s1y + s1x * s2y
    s = (-s1y * (p1x - q1x) + s1x * (p1y - q1y)) / det
    t = ( s2x * (p1y - q1y) - s2y * (p1x - q1x)) / det
    intersection = P $ V2 (p1x + (t * s1x)) (p1y + (t * s1y))

isIntersectSegSeg :: DLine -> DLine -> Bool
-- isIntersectSegSeg (a,b) (c,d) = ( (ccw a c d) /= (ccw b c d ) ) && ( (ccw a b c) /= (ccw a b d) )
--   where ccw (P (V2 ax ay)) (P (V2 bx by)) (P (V2 cx cy)) =
--           (cy-ay)*(bx-ax) > (by-ay)*(cx-ax)
isIntersectSegSeg l1 l2 =
  case intersectSegSeg l1 l2 of
    SSOneIntersect _ -> True
    SSNoIntersect    -> False

intersectSegSegs :: DLine -> [DLine] -> IntersectSegConvex
intersectSegSegs l = foldr intersectSegSegs' SCNoIntersect
  where
    intersectSegSegs' _  i@SCTwoIntersect{}       = i
    intersectSegSegs' l' i@(SCOneIntersect p1 l1) =
      case intersectSegSeg l l' of
        SSOneIntersect p -> SCTwoIntersect p1 l1 p l'
        SSNoIntersect    -> i
    intersectSegSegs' l' i@SCNoIntersect =
      case intersectSegSeg l l' of
        SSOneIntersect p -> SCOneIntersect p l'
        SSNoIntersect    -> i

intersectSegSweep :: DLine -> DLine -> DVec -> IntersectSegConvex
intersectSegSweep l sl@(sp1, sp2) sv = intersectSegSegs l segs
  where
    segs = [(sp1, sp2), (sp1, sp3), (sp2, sp4), (sp3, sp4)]
    (sp3, sp4) = segTranslate sv sl

pointInsideSweep :: DPoint -> DLine -> DVec -> Bool
pointInsideSweep (P (V2 x y)) (P (V2 p1x p1y), P (V2 p2x p2y)) (V2 dx dy) =
  (abs det > 1e-20) && (bb >= 0) && (cc >= 0) && (bb <= 1) && (cc <= 1)
  where
    xb = p1x - p2x
    yb = p1y - p2y
    xc = dx
    yc = dy
    xp = x - p2x
    yp = y - p2y
    det  = xb * yc - yb * xc
    oned = 1.0 / det
    bb   = (xp * yc - xc * yp) * oned
    cc   = (xb * yp - xp * yb) * oned

intersectSegCircle :: DLine -> DCircle -> IntersectSegConvex
intersectSegCircle l@(p1, _) (p, r)
  | discriminant < 0 = SCNoIntersect
  | t1 >= 0 && t1 <= 1 =
      if t2 >= 0 && t2 <= 1 then
        SCTwoIntersect ip1 l ip2 l
      else
        SCOneIntersect ip1 l
  | t2 >= 0 && t2 <= 1 = SCOneIntersect ip2 l
  | otherwise = SCNoIntersect
  where
    discriminant = b*b - 4*a*c
    d = segVec l
    f = segVec (p, p1)
    a = d `dot` d
    b = 2*f `dot` d
    c = f `dot` f - r * r
    sd = sqrt discriminant
    t1 = ((-b) - sd) / (2*a)
    t2 = ((-b) + sd) / (2*a)
    ip1 = p1 + (P $ t1 *^ d)
    ip2 = p1 + (P $ t2 *^ d)

pointInsideCircle :: DPoint -> DCircle -> Bool
pointInsideCircle p (cp, cr) = segLen (p, cp) < cr

isIntersectSegCircle :: DLine -> DCircle -> Bool
isIntersectSegCircle l c =
  case intersectSegCircle l c of
    SCNoIntersect -> False
    _             -> True

isConvex :: [DLine] -> Bool
isConvex [] = False
isConvex [_] = False
isConvex ls@(_:lt) = all (>0) crosses || all (<0) crosses
  where
    crosses = zipWith angle ls (lt ++ ls)
    angle l1 l2 = crossZ (segVec l1) (segVec l2)

innerSegments :: Double -> [DLine] -> [DLine]
innerSegments _ []     = []
innerSegments _ [_]    = []
innerSegments shift (sh:st) = closedSegs $ last innerPoints : init innerPoints -- rotate back
  where
    innerSegments'h = segTranslatePerp (-shift) sh
    innerSegments't = fmap (segTranslatePerp (-shift)) st
    innerSegments' = innerSegments'h:innerSegments't
    innerIntersections = zipWith intersectSegSeg innerSegments' (innerSegments't ++ innerSegments')
    innerPoints = [ p | SSOneIntersect p <- innerIntersections ]
