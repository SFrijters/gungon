{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module GunGon ( main ) where

import           Control.Lens             ((^.))
import           Control.Monad            (unless)
import           Control.Monad.IO.Class   (MonadIO (..))
import           Control.Monad.Logger     (LogLevel, LoggingT, MonadLogger,
                                           filterLogger, runStdoutLoggingT)
import           Control.Monad.Reader     (MonadReader, ReaderT, runReaderT)
import           Data.Time.Clock          (getCurrentTime)

import           GunGon.Config.GameConfig
import           GunGon.Config.OptParse
import           GunGon.Render
import           GunGon.State.InputState
import           GunGon.State.SceneState
import           GunGon.State.Types

main :: IO ()
main = do
  options <- getCmdOptions
  if options^.doShowVersion
    then
      printVersion
    else do
      gameConfig <- readGameConfig (options^.configPath)
      case gameConfig of
        Right gameConfig' ->
          if options^.doCheckConfig then putStrLn "Valid configuration" else do
            -- Initialize the SDL renderer and font library
            gr <- GunGon.Render.initialize (options^.doDebug)
            sceneSize' <- getSceneSize gr
            -- Start game loop
            t0 <- getCurrentTime
            runGunGon (options^.logLevel) gameConfig' $ mainLoop gr (mkInitialSceneState gameConfig' sceneSize') (mkInitialInputState t0)
            -- Cleanup for SDL
            GunGon.Render.destroy gr
        Left jsonError -> putStrLn jsonError

newtype GunGon a = GunGon (LoggingT (ReaderT GameConfig IO) a)
  deriving (Functor, Applicative, Monad, MonadReader GameConfig, MonadLogger, MonadIO)

runGunGon :: LogLevel -> GameConfig -> GunGon a -> IO a
runGunGon logLevel_ gameConfig_ (GunGon m) =
  runReaderT (runStdoutLoggingT $ filterLogger (\_ lvl -> lvl >= logLevel_) m) gameConfig_

mainLoop :: (MonadReader GameConfig m, MonadLogger m, MonadIO m) => GRenderer -> SceneState -> InputState -> m ()
mainLoop gRenderer_ sceneState_ inputState_ = do

  inputState' <- updateInputState inputState_ =<< pollEvents

  sceneSize' <- getSceneSize gRenderer_

  sceneState' <- ssStep inputState' sceneSize' sceneState_

  gRenderer' <- setCameraOffset sceneState' gRenderer_

  runRenderer gRenderer' $ renderScene sceneState' (recip . realToFrac $ inputState'^.diffTime)

  unless (sceneState'^.scene == SceneQuit) $ mainLoop gRenderer' sceneState' inputState'
