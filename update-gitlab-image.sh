#!/usr/bin/env bash

# TODO: Put this inside the flake as well

set -Eeuo pipefail

if ! skopeo --version >/dev/null 2>&1; then
    echo "Unable to find 'skopeo', enter 'nix develop .#gungon-podman' first."
    exit 1
fi

export CI_REGISTRY=registry.gitlab.com
# export CI_REGISTRY_USER=foo
# export CI_REGISTRY_PASSWORD=bar
if [ -f ./gitlab-container-registry-credentials.sh ]; then
    # Needs Developer role and read_registry and write_registry scopes
    source ./gitlab-container-registry-credentials.sh
fi

registry_user="$(echo "${CI_REGISTRY_USER}" | tr '[:upper:]' '[:lower:]')"
image_name=gungon
image_tag=nix
remote_image_uri="docker://${CI_REGISTRY}/${registry_user}/${image_name}:${image_tag}"

echo "Logging into the registry with skopeo"
skopeo login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
echo "Building and streaming image '.#gungon-dev-container-stream' to '${remote_image_uri}'"
eval "$(nix build .#gungon-dev-container-stream --print-out-paths --no-link)" | gzip --fast | skopeo copy docker-archive:/dev/stdin "${remote_image_uri}"
