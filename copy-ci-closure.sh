#!/usr/bin/env bash

set -Eeuo pipefail

if [ $# != 1 ]; then
  echo "ERROR: needs remote server as the only argument"
  exit 1
fi

server=${1}

nix-copy-closure --to "${server}" "$(nix build .#gungon-no-tests --no-link --print-out-paths)"
